/*
Navicat MySQL Data Transfer

Source Server         : 服务器上的
Source Server Version : 50167
Source Host           : 42.121.108.228:3306
Source Database       : rainbow

Target Server Type    : MYSQL
Target Server Version : 50167
File Encoding         : 65001

Date: 2014-07-21 18:43:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for Card_Recharge_History
-- ----------------------------
DROP TABLE IF EXISTS `Card_Recharge_History`;
CREATE TABLE `Card_Recharge_History` (
  `nRecordId` int(11) NOT NULL AUTO_INCREMENT,
  `nMembershipId` int(11) DEFAULT NULL,
  `strMembershipCardMerchant` varchar(20) DEFAULT NULL,
  `strRechargeMerchant` varchar(20) DEFAULT NULL,
  `strRechargeSum` varchar(20) DEFAULT NULL,
  `strRechargeSumNow` varchar(20) DEFAULT NULL,
  `strOperator` varchar(20) DEFAULT NULL,
  `dtOperateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_vip_charge` (`nMembershipId`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Card_Recharge_History
-- ----------------------------
INSERT INTO `Card_Recharge_History` VALUES ('1', '10001', '桂祥大酒店', '桂祥大酒店', '10000', '9999.99', '应当', '2014-07-16 14:14:18');
INSERT INTO `Card_Recharge_History` VALUES ('5', '10001', '桂祥大酒店', '桂祥大酒店', '10000', '9999.99', '应当', '2014-07-16 14:14:18');
INSERT INTO `Card_Recharge_History` VALUES ('4', '10001', '桂祥大酒店', '桂祥大酒店', '10000', '9999.99', '应当', '2014-07-16 14:14:18');
INSERT INTO `Card_Recharge_History` VALUES ('3', '10001', '桂祥大酒店', '桂祥大酒店', '10000', '9999.99', '应当', '2014-07-16 14:14:18');
INSERT INTO `Card_Recharge_History` VALUES ('2', '10001', '桂祥大酒店', '桂祥大酒店', '10000', '9999.99', '应当', '2014-07-16 14:14:18');

-- ----------------------------
-- Table structure for Card_Replace_History
-- ----------------------------
DROP TABLE IF EXISTS `Card_Replace_History`;
CREATE TABLE `Card_Replace_History` (
  `nRecordId` int(11) NOT NULL AUTO_INCREMENT,
  `nMembershipId` int(11) DEFAULT NULL,
  `strPreMenbershipCardId` varchar(20) DEFAULT NULL,
  `strNowMembershipCardId` varchar(20) DEFAULT NULL,
  `strReplacingReason` text,
  `dtReplacingTime` datetime DEFAULT NULL,
  `strOperator` varchar(20) DEFAULT NULL,
  `dtOperateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_vip_to_many_card_change` (`nMembershipId`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Card_Replace_History
-- ----------------------------
INSERT INTO `Card_Replace_History` VALUES ('1', '10001', '4332211', '1122334', '原卡丢失', '2014-07-18 14:17:37', '应当', '2014-07-18 14:17:56');
INSERT INTO `Card_Replace_History` VALUES ('5', '10001', '4332211', '1122334', '原卡丢失', '2014-07-18 14:17:37', '应当', '2014-07-18 14:17:56');
INSERT INTO `Card_Replace_History` VALUES ('4', '10001', '4332211', '1122334', '原卡丢失', '2014-07-18 14:17:37', '应当', '2014-07-18 14:17:56');
INSERT INTO `Card_Replace_History` VALUES ('3', '10001', '4332211', '1122334', '原卡丢失', '2014-07-18 14:17:37', '应当', '2014-07-18 14:17:56');
INSERT INTO `Card_Replace_History` VALUES ('2', '10001', '4332211', '1122334', '原卡丢失', '2014-07-18 14:17:37', '应当', '2014-07-18 14:17:56');

-- ----------------------------
-- Table structure for Comment
-- ----------------------------
DROP TABLE IF EXISTS `Comment`;
CREATE TABLE `Comment` (
  `nRecordId` int(11) NOT NULL AUTO_INCREMENT,
  `nMembershipId` int(11) DEFAULT NULL,
  `nAccountId` int(11) DEFAULT NULL,
  `strComment` text,
  `strCommentIP` varchar(20) DEFAULT NULL,
  `srrCommentName` varchar(20) DEFAULT NULL,
  `dtCommentTime` datetime DEFAULT NULL,
  `bEffectived` tinyint(1) DEFAULT NULL,
  `bReply` tinyint(1) DEFAULT NULL,
  `strReply_man` varchar(20) DEFAULT NULL,
  `strCommentReply` varchar(20) DEFAULT NULL,
  `dtReply` datetime DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_merchant_to_be_comment` (`nAccountId`),
  KEY `FK_vipComment` (`nMembershipId`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Comment
-- ----------------------------
INSERT INTO `Comment` VALUES ('1', '10001', '1234567', '房间舒适，早餐丰富，是我见过最好的酒店，不住不是中国人。', '118.114.138.241', '欧阳小明', '2014-07-23 09:21:26', '1', '1', '应当', '谢谢，欢迎再次光临本店', '2014-07-23 09:22:27');
INSERT INTO `Comment` VALUES ('5', '10001', '1234567', '房间舒适，早餐丰富，是我见过最好的酒店，不住不是中国人。', '118.114.138.241', '欧阳小明', '2014-07-23 09:21:26', '1', '1', '应当', '谢谢，欢迎再次光临本店', '2014-07-23 09:22:27');
INSERT INTO `Comment` VALUES ('4', '10001', '1234567', '房间舒适，早餐丰富，是我见过最好的酒店，不住不是中国人。', '118.114.138.241', '欧阳小明', '2014-07-23 09:21:26', '1', '1', '应当', '谢谢，欢迎再次光临本店', '2014-07-23 09:22:27');
INSERT INTO `Comment` VALUES ('3', '10001', '1234567', '房间舒适，早餐丰富，是我见过最好的酒店，不住不是中国人。', '118.114.138.241', '欧阳小明', '2014-07-23 09:21:26', '1', '1', '应当', '谢谢，欢迎再次光临本店', '2014-07-23 09:22:27');
INSERT INTO `Comment` VALUES ('2', '10001', '1234567', '房间舒适，早餐丰富，是我见过最好的酒店，不住不是中国人。', '118.114.138.241', '欧阳小明', '2014-07-23 09:21:26', '1', '1', '应当', '谢谢，欢迎再次光临本店', '2014-07-23 09:22:27');

-- ----------------------------
-- Table structure for Commodity
-- ----------------------------
DROP TABLE IF EXISTS `Commodity`;
CREATE TABLE `Commodity` (
  `nCommodityId` int(11) NOT NULL AUTO_INCREMENT,
  `nAccountId` int(11) DEFAULT NULL,
  `strCommodityName` varchar(20) DEFAULT NULL,
  `fPrice` decimal(20,0) DEFAULT NULL,
  `fVIPPrice` decimal(20,0) DEFAULT NULL,
  `strIntroduce` text,
  `imaCommodityPicture` longblob,
  `strRoomType` varchar(20) DEFAULT NULL,
  `strBreakfast` varchar(20) DEFAULT NULL,
  `strBedType` varchar(20) DEFAULT NULL,
  `strInterner` varchar(20) DEFAULT NULL,
  `bRoomCondition` tinyint(1) DEFAULT NULL,
  `fASPOTPrice` decimal(10,0) DEFAULT NULL,
  `fWeekendPrice` decimal(10,0) DEFAULT NULL,
  `bCondition` tinyint(1) DEFAULT NULL,
  `strRemarks` text,
  `nState` int(11) DEFAULT NULL,
  PRIMARY KEY (`nCommodityId`),
  KEY `FK_Merchant2Product` (`nAccountId`)
) ENGINE=MyISAM AUTO_INCREMENT=1003 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Commodity
-- ----------------------------
INSERT INTO `Commodity` VALUES ('1001', '1234567', '桂祥大酒店', '238', '120', '桂祥大酒店是一家高档商务酒店，酒店装修时尚高雅，设施齐全，环境舒适。拥有跃层商务客房，酒店还配有豪华餐饮包厢、中西自助餐厅、会议厅、商务中心、精品屋、美容美发和足浴中心等，服务配套与娱乐设施一应俱全。', null, '单人间，双人间，豪华间，标准间，商务间', '什锦冻肉肠仔镜盘，中式冷拼，沙拉吧', '单人床，双人床', '10M宽带', '1', '200', '200', '1', '无', '1');
INSERT INTO `Commodity` VALUES ('1002', '2345678', '希尔顿酒店', '2380', '1200', '希尔顿国际酒店集团，为总部设于英国的希尔顿集团公司旗下分支，拥有除美国外全球范围内“希尔顿”商标的使用权。', null, '单人间，双人间，豪华间，标准间，商务间', '西瓜、菠萝、白兰瓜，麦片粥，法式吐司', '单人床，双人床', '100M宽带', '1', '2000', '2000', '1', '无', '1');

-- ----------------------------
-- Table structure for Inside_Message
-- ----------------------------
DROP TABLE IF EXISTS `Inside_Message`;
CREATE TABLE `Inside_Message` (
  `nRecordId` int(20) NOT NULL AUTO_INCREMENT,
  `strTitleMessage` varchar(20) DEFAULT NULL,
  `strContent` text,
  `strSender` varchar(20) DEFAULT NULL,
  `strReceivor` varchar(20) DEFAULT NULL,
  `bCondition` tinyint(1) DEFAULT NULL,
  `strHandle_Man` varchar(20) DEFAULT NULL,
  `dtHandle_Time` datetime DEFAULT NULL,
  PRIMARY KEY (`nRecordId`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Inside_Message
-- ----------------------------
INSERT INTO `Inside_Message` VALUES ('1', '缴费通知', '尊敬的会员，您于2014年7月17日成功缴费0.1元，卡上余额10000.', '七彩云平台', '希尔顿酒店', '0', '小明', '2014-07-20 08:35:56');
INSERT INTO `Inside_Message` VALUES ('2', '入盟通知', '恭喜您，您已经是七彩云平台的会员啦，快和其他小伙伴打个招呼吧！', '七彩云平台', '桂祥大酒店', '1', '小红', '2014-07-21 08:38:59');
INSERT INTO `Inside_Message` VALUES ('3', '入盟通知', '恭喜您，您已经是七彩云平台的会员啦，快和其他小伙伴打个招呼吧！', '七彩云平台', '桂祥大酒店', '1', '小红', '2014-07-21 08:38:59');
INSERT INTO `Inside_Message` VALUES ('4', '入盟通知', '恭喜您，您已经是七彩云平台的会员啦，快和其他小伙伴打个招呼吧！', '七彩云平台', '桂祥大酒店', '1', '小红', '2014-07-21 08:38:59');
INSERT INTO `Inside_Message` VALUES ('5', '入盟通知', '恭喜您，您已经是七彩云平台的会员啦，快和其他小伙伴打个招呼吧！', '七彩云平台', '桂祥大酒店', '1', '小红', '2014-07-21 08:38:59');

-- ----------------------------
-- Table structure for Log_Record
-- ----------------------------
DROP TABLE IF EXISTS `Log_Record`;
CREATE TABLE `Log_Record` (
  `nRecordId` int(11) NOT NULL AUTO_INCREMENT,
  `nAccountId` int(11) DEFAULT NULL,
  `dtTime` datetime DEFAULT NULL,
  `bType` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_log_to_account` (`nAccountId`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Log_Record
-- ----------------------------
INSERT INTO `Log_Record` VALUES ('1', '1234567', '2014-07-22 08:44:38', '1');
INSERT INTO `Log_Record` VALUES ('2', '2345678', '2014-07-23 08:45:11', '1');
INSERT INTO `Log_Record` VALUES ('3', '2345678', '2014-07-22 08:44:38', '1');
INSERT INTO `Log_Record` VALUES ('4', '2345678', '2014-07-22 08:44:38', '1');
INSERT INTO `Log_Record` VALUES ('5', '2345678', '2014-07-22 08:44:38', '1');

-- ----------------------------
-- Table structure for Manager
-- ----------------------------
DROP TABLE IF EXISTS `Manager`;
CREATE TABLE `Manager` (
  `nUserId` int(11) NOT NULL AUTO_INCREMENT,
  `strPosition` char(20) DEFAULT NULL,
  `bSex` tinyint(1) DEFAULT NULL,
  `strName` char(20) DEFAULT NULL,
  `strPasswd` char(20) DEFAULT NULL,
  `strAddress` char(20) DEFAULT NULL,
  PRIMARY KEY (`nUserId`)
) ENGINE=MyISAM AUTO_INCREMENT=234571 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Manager
-- ----------------------------
INSERT INTO `Manager` VALUES ('123456', '项目经理', '1', '小小明', '123456', '四川省成都市尚锦路89号');
INSERT INTO `Manager` VALUES ('0', '产品经理', '2', '小小红', '234567', '四川省成都市尚锦路88号');
INSERT INTO `Manager` VALUES ('123457', '产品经理', '1', '小小红', '234567', '四川省成都市尚锦路88号');

-- ----------------------------
-- Table structure for Merchant_Account_Info
-- ----------------------------
DROP TABLE IF EXISTS `Merchant_Account_Info`;
CREATE TABLE `Merchant_Account_Info` (
  `nAccountId` int(11) NOT NULL AUTO_INCREMENT,
  `strMerchantId` char(20) DEFAULT NULL,
  `nRecordId` int(11) DEFAULT NULL,
  `VIP_nRecordId` int(20) DEFAULT NULL,
  `nUserId` int(11) DEFAULT NULL,
  `strName` char(20) DEFAULT NULL,
  `strPasswd` varchar(20) DEFAULT NULL,
  `nAccountType` int(11) DEFAULT NULL,
  `dtLastLogTime` datetime DEFAULT NULL,
  `bInUse` tinyint(1) DEFAULT NULL,
  `strOperator` varchar(20) DEFAULT NULL,
  `dtOperateTime` datetime DEFAULT NULL,
  `nPointsRate` int(11) DEFAULT NULL,
  `bIsDisInHome` tinyint(1) DEFAULT NULL,
  `bIsRecommend` tinyint(1) DEFAULT NULL,
  `strType` char(20) DEFAULT NULL,
  `strFrontIP` char(20) DEFAULT NULL,
  `strVerificationCode` char(20) DEFAULT NULL,
  PRIMARY KEY (`nAccountId`),
  KEY `FK_account_to_detail2` (`nRecordId`),
  KEY `FK_manager_merchant_mapping` (`nUserId`),
  KEY `FK_merchant2receive_info` (`VIP_nRecordId`),
  KEY `FK_reg_merchant2` (`strMerchantId`)
) ENGINE=MyISAM AUTO_INCREMENT=2345682 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Merchant_Account_Info
-- ----------------------------
INSERT INTO `Merchant_Account_Info` VALUES ('1234567', '1', '1', '1', '123456', '桂祥大酒店', 'root', '1', '2014-07-25 09:56:29', '1', '令狐小明', '2014-07-25 09:57:09', '50', '1', '1', '酒店', '118.114.138.278', '110245');
INSERT INTO `Merchant_Account_Info` VALUES ('2345678', '2', '2', '2', '123457', '希尔顿酒店', 'axiba', '1', '2014-07-25 09:59:27', '1', '令狐小红', '2014-07-25 10:00:09', '40', '1', '1', '酒店', '118.114.138.237', '229874');

-- ----------------------------
-- Table structure for Merchant_Detail
-- ----------------------------
DROP TABLE IF EXISTS `Merchant_Detail`;
CREATE TABLE `Merchant_Detail` (
  `nRecordId` int(11) NOT NULL AUTO_INCREMENT,
  `nAccountId` int(11) DEFAULT NULL,
  `strMerchantIntroduce` text,
  `imaMerchantPicture` longblob,
  `bMainpictureOrNot` tinyint(1) DEFAULT NULL,
  `strAdress` varchar(20) DEFAULT NULL,
  `strPhone` varchar(11) DEFAULT NULL,
  `strTelautogram` varchar(20) DEFAULT NULL,
  `strWebsite` varchar(100) DEFAULT NULL,
  `strEmail` varchar(20) DEFAULT NULL,
  `strName` varchar(20) DEFAULT NULL,
  `nStar` int(11) DEFAULT NULL,
  `strProvince` varchar(20) DEFAULT NULL,
  `strCity` varchar(20) DEFAULT NULL,
  `strTown` varchar(20) DEFAULT NULL,
  `strLandmark` varchar(20) DEFAULT NULL,
  `strMemberPreference` varchar(20) DEFAULT NULL,
  `strSalesPromotion` varchar(20) DEFAULT NULL,
  `booCommodityPicture` tinyint(1) DEFAULT NULL,
  `strMenberPrice` varchar(20) DEFAULT NULL,
  `strMerchantName` varchar(20) DEFAULT NULL,
  `nState` int(11) DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_account_to_detail` (`nAccountId`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Merchant_Detail
-- ----------------------------
INSERT INTO `Merchant_Detail` VALUES ('1', '1234567', '桂祥大酒店是一家高档商务酒店，酒店装修时尚高雅，设施齐全，环境舒适。拥有跃层商务客房，酒店还配有豪华餐饮包厢、中西自助餐厅、会议厅、商务中心、精品屋、美容美发和足浴中心等，服务配套与娱乐设施一应俱全。', null, '1', '四川省成都市尚锦路89号', '4006994587', ' 63380800', 'www.gxdjd.com', 'gxdjd@163.com', '桂祥大酒店', '5', '四川', '成都市', '郫县', '桂祥大厦', '100元', '150元起', '1', '100元', '桂祥大酒店', '1');
INSERT INTO `Merchant_Detail` VALUES ('2', '2345678', '希尔顿国际酒店集团，为总部设于英国的希尔顿集团公司旗下分支，拥有除美国外全球范围内“希尔顿”商标的使用权。', null, '1', '四川省成都市天府大道中段666号', '4008203722', '63380811', 'www.xedjd.com', 'xed@163.com', '希尔顿酒店', '5', '四川', '成都市', '成都', '希尔顿广场', '1000', '1500元起', '1', '1000元', '希尔顿酒店', '1');
INSERT INTO `Merchant_Detail` VALUES ('3', '2345678', '希尔顿国际酒店集团，为总部设于英国的希尔顿集团公司旗下分支，拥有除美国外全球范围内“希尔顿”商标的使用权。', null, '1', '四川省成都市天府大道中段666号', '4008203722', '63380811', 'www.xedjd.com', 'xed@163.com', '希尔顿酒店', '5', '四川', '成都市', '成都', '希尔顿广场', '1000', '1500元起', '1', '1000元', '希尔顿酒店', '1');
INSERT INTO `Merchant_Detail` VALUES ('4', '2345678', '希尔顿国际酒店集团，为总部设于英国的希尔顿集团公司旗下分支，拥有除美国外全球范围内“希尔顿”商标的使用权。', null, '1', '四川省成都市天府大道中段666号', '4008203722', '63380811', 'www.xedjd.com', 'xed@163.com', '希尔顿酒店', '5', '四川', '成都市', '成都', '希尔顿广场', '1000', '1500元起', '1', '1000元', '希尔顿酒店', '1');
INSERT INTO `Merchant_Detail` VALUES ('5', '2345678', '希尔顿国际酒店集团，为总部设于英国的希尔顿集团公司旗下分支，拥有除美国外全球范围内“希尔顿”商标的使用权。', null, '1', '四川省成都市天府大道中段666号', '4008203722', '63380811', 'www.xedjd.com', 'xed@163.com', '希尔顿酒店', '5', '四川', '成都市', '成都', '希尔顿广场', '1000', '1500元起', '1', '1000元', '希尔顿酒店', '1');

-- ----------------------------
-- Table structure for Merchant_Keyword
-- ----------------------------
DROP TABLE IF EXISTS `Merchant_Keyword`;
CREATE TABLE `Merchant_Keyword` (
  `nRecordId` int(20) NOT NULL AUTO_INCREMENT,
  `strCityName` varchar(20) DEFAULT NULL,
  `strIndustry` varchar(20) DEFAULT NULL,
  `strKeywoed` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`nRecordId`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Merchant_Keyword
-- ----------------------------
INSERT INTO `Merchant_Keyword` VALUES ('1', '成都市', '餐饮业', '桂祥');
INSERT INTO `Merchant_Keyword` VALUES ('2', '成都市', '餐饮业', '希尔顿');
INSERT INTO `Merchant_Keyword` VALUES ('3', '成都市', '餐饮业', '希尔顿');
INSERT INTO `Merchant_Keyword` VALUES ('4', '成都市', '餐饮业', '希尔顿');
INSERT INTO `Merchant_Keyword` VALUES ('5', '成都市', '餐饮业', '希尔顿');

-- ----------------------------
-- Table structure for Merchant_Reg_Info
-- ----------------------------
DROP TABLE IF EXISTS `Merchant_Reg_Info`;
CREATE TABLE `Merchant_Reg_Info` (
  `strMerchantId` char(20) NOT NULL,
  `nAccountId` int(11) DEFAULT NULL,
  `strFMerchantId` char(20) DEFAULT NULL,
  `strSMerchantId` char(20) DEFAULT NULL,
  `strMerchantType` char(20) DEFAULT NULL,
  `strMerchantName` char(20) DEFAULT NULL,
  `strAdress` char(20) DEFAULT NULL,
  `strMerchantPhone` char(11) DEFAULT NULL,
  `strName` char(20) DEFAULT NULL,
  `strZipcode` char(20) DEFAULT NULL,
  `nStar` int(11) DEFAULT NULL,
  `nComsunPtionpratio` int(11) DEFAULT NULL,
  `nMemberDiscRatio` int(11) DEFAULT NULL,
  `strTitle` char(20) DEFAULT NULL,
  `strPosition` char(20) DEFAULT NULL,
  `strPhone` char(11) DEFAULT NULL,
  `strCellphone` char(20) DEFAULT NULL,
  `strTelautogram` char(20) DEFAULT NULL,
  `strEmail` char(20) DEFAULT NULL,
  PRIMARY KEY (`strMerchantId`),
  KEY `FK_reg_merchant` (`nAccountId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Merchant_Reg_Info
-- ----------------------------
INSERT INTO `Merchant_Reg_Info` VALUES ('1', '1234567', '1111', '111111', '酒店', '桂祥大酒店', '四川省成都市尚锦路89号', '4006994587', '桂祥大酒店', '610000', '5', '50', '50', '赵小姐', '大堂经理', '4006994587', '18624657845', '63380800', 'gxdjd@163.com');
INSERT INTO `Merchant_Reg_Info` VALUES ('2', '2345678', '2222', '222222', '酒店', '希尔顿酒店', '四川省成都市天府大道中段666号', '4008203722', '希尔顿酒店', '610000', '5', '40', '40', '李小姐', '大堂经理', '4008203722', '15946723584', '63380811', 'xedjd@163.com');

-- ----------------------------
-- Table structure for News_Show
-- ----------------------------
DROP TABLE IF EXISTS `News_Show`;
CREATE TABLE `News_Show` (
  `nRecordId` int(20) NOT NULL AUTO_INCREMENT,
  `strNewsTitle` varchar(20) DEFAULT NULL,
  `strNewsContent` text,
  `strPublisher` varchar(20) DEFAULT NULL,
  `dtPublicTime` datetime DEFAULT NULL,
  PRIMARY KEY (`nRecordId`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of News_Show
-- ----------------------------
INSERT INTO `News_Show` VALUES ('1', '桂祥大酒店节日大降价', '为了迎接愚人节的到来，桂祥大酒店特推出愚人节特别套餐，50元起的价格，一流的体验来感谢新老客户的支持。谢谢', '桂祥大酒店', '2014-07-26 10:42:50');
INSERT INTO `News_Show` VALUES ('2', '希尔顿酒店会员价格调整声明', '为感谢广大新老客户的支持与厚爱，希尔顿酒店特别推出新的会员价格标准，详情见附件。谢谢', '希尔顿酒店', '2014-07-26 10:44:51');
INSERT INTO `News_Show` VALUES ('3', '希尔顿酒店会员价格调整声明', '为感谢广大新老客户的支持与厚爱，希尔顿酒店特别推出新的会员价格标准，详情见附件。谢谢', '希尔顿酒店', '2014-07-26 10:44:51');
INSERT INTO `News_Show` VALUES ('4', '希尔顿酒店会员价格调整声明', '为感谢广大新老客户的支持与厚爱，希尔顿酒店特别推出新的会员价格标准，详情见附件。谢谢', '希尔顿酒店', '2014-07-26 10:44:51');
INSERT INTO `News_Show` VALUES ('5', '希尔顿酒店会员价格调整声明', '为感谢广大新老客户的支持与厚爱，希尔顿酒店特别推出新的会员价格标准，详情见附件。谢谢', '希尔顿酒店', '2014-07-26 10:44:51');

-- ----------------------------
-- Table structure for Operate_Record
-- ----------------------------
DROP TABLE IF EXISTS `Operate_Record`;
CREATE TABLE `Operate_Record` (
  `nRecordId` int(11) NOT NULL AUTO_INCREMENT,
  `nAccountId` int(11) DEFAULT NULL,
  `dtTime` datetime DEFAULT NULL,
  `strURL` text,
  `strFunctionName` text,
  `strOperateIntroduction` text,
  `strRemark` text,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_account_to_operate` (`nAccountId`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Operate_Record
-- ----------------------------
INSERT INTO `Operate_Record` VALUES ('1', '1234567', '2014-07-27 10:49:54', 'www.gxdjd.com', 'add', '增加一条信息', '无');
INSERT INTO `Operate_Record` VALUES ('2', '2345678', '2014-07-27 10:50:32', 'www.xedjd.com', 'delete', '删除一条信息', '无');
INSERT INTO `Operate_Record` VALUES ('3', '2345678', '2014-07-27 10:50:32', 'www.xedjd.com', 'delete', '删除一条信息', '无');
INSERT INTO `Operate_Record` VALUES ('4', '2345678', '2014-07-27 10:50:32', 'www.xedjd.com', 'delete', '删除一条信息', '无');
INSERT INTO `Operate_Record` VALUES ('5', '2345678', '2014-07-27 10:50:32', 'www.xedjd.com', 'delete', '删除一条信息', '无');

-- ----------------------------
-- Table structure for Points_To_Cash
-- ----------------------------
DROP TABLE IF EXISTS `Points_To_Cash`;
CREATE TABLE `Points_To_Cash` (
  `nRecordId` int(20) NOT NULL AUTO_INCREMENT,
  `nMembershipId` int(11) DEFAULT NULL,
  `nCostPoints` int(11) DEFAULT NULL,
  `dAmountOfCash` decimal(10,0) DEFAULT NULL,
  `strWayOfReceive` varchar(20) DEFAULT NULL,
  `strBankAccount` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_points2cash` (`nMembershipId`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Points_To_Cash
-- ----------------------------
INSERT INTO `Points_To_Cash` VALUES ('1', '10001', '1000', '10', '银行转账', '1287648759874097481');
INSERT INTO `Points_To_Cash` VALUES ('5', '10001', '1000', '10', '银行转账', '1287648759874097481');
INSERT INTO `Points_To_Cash` VALUES ('4', '10001', '1000', '10', '银行转账', '1287648759874097481');
INSERT INTO `Points_To_Cash` VALUES ('3', '10001', '1000', '10', '银行转账', '1287648759874097481');
INSERT INTO `Points_To_Cash` VALUES ('2', '10001', '1000', '10', '银行转账', '1287648759874097481');

-- ----------------------------
-- Table structure for Points_To_Present
-- ----------------------------
DROP TABLE IF EXISTS `Points_To_Present`;
CREATE TABLE `Points_To_Present` (
  `nRecordId` int(11) NOT NULL AUTO_INCREMENT,
  `nMembershipId` int(11) DEFAULT NULL,
  `nPresentCostPoints` int(11) DEFAULT NULL,
  `nQuantity` int(11) DEFAULT NULL,
  `strPresentName` varchar(20) DEFAULT NULL,
  `strWayOfReceive` varchar(20) DEFAULT NULL,
  `strMenbershipAdress` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `AK_Identifier_1` (`nPresentCostPoints`),
  KEY `FK_vipGetPresent` (`nMembershipId`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Points_To_Present
-- ----------------------------
INSERT INTO `Points_To_Present` VALUES ('1', '10001', '4000', '1', '芭比娃娃', '快递', '四川省成都市尚锦路89号');
INSERT INTO `Points_To_Present` VALUES ('3', '10001', '4000', '1', '芭比娃娃', '快递', '四川省成都市尚锦路89号');
INSERT INTO `Points_To_Present` VALUES ('4', '10001', '4000', '1', '芭比娃娃', '快递', '四川省成都市尚锦路89号');
INSERT INTO `Points_To_Present` VALUES ('5', '10001', '4000', '1', '芭比娃娃', '快递', '四川省成都市尚锦路89号');
INSERT INTO `Points_To_Present` VALUES ('2', '10001', '4000', '1', '芭比娃娃', '快递', '四川省成都市尚锦路89号');

-- ----------------------------
-- Table structure for Pre_Maked_Card
-- ----------------------------
DROP TABLE IF EXISTS `Pre_Maked_Card`;
CREATE TABLE `Pre_Maked_Card` (
  `strMembershipCardId` varchar(20) NOT NULL,
  `nMembershipId` int(11) DEFAULT NULL,
  `nAccountId` int(11) DEFAULT NULL,
  `bCardCondition` tinyint(1) DEFAULT NULL,
  `strCardMaker` varchar(20) DEFAULT NULL,
  `dtCardMakeTime` datetime DEFAULT NULL,
  `strCardSender` varchar(20) DEFAULT NULL,
  `dtCardSendTime` datetime DEFAULT NULL,
  PRIMARY KEY (`strMembershipCardId`),
  KEY `FK_card_to_vip2` (`nMembershipId`),
  KEY `FK_merchant_to_many_card` (`nAccountId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Pre_Maked_Card
-- ----------------------------
INSERT INTO `Pre_Maked_Card` VALUES ('1122334', '10001', '1234567', '1', '百里小明', '2014-07-28 11:02:58', '百里小明', '2014-07-29 11:03:23');
INSERT INTO `Pre_Maked_Card` VALUES ('', null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for Proposal
-- ----------------------------
DROP TABLE IF EXISTS `Proposal`;
CREATE TABLE `Proposal` (
  `nRecordId` int(20) NOT NULL AUTO_INCREMENT,
  `nAccountId` int(11) DEFAULT NULL,
  `strTitle` char(20) DEFAULT NULL,
  `strContent` char(20) DEFAULT NULL,
  `dtAddTime` datetime DEFAULT NULL,
  `strAddPerson` char(20) DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_merchant_to_many_proposal` (`nAccountId`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Proposal
-- ----------------------------
INSERT INTO `Proposal` VALUES ('1', '1234567', '早餐种类不丰富', '希望早餐种类可以再丰富些', '2014-07-30 11:11:22', '端木小明');
INSERT INTO `Proposal` VALUES ('2', '1234567', '早餐种类不丰富', '早餐种类不丰富', '2014-07-30 11:11:22', '端木小红');
INSERT INTO `Proposal` VALUES ('3', '1234567', '早餐种类不丰富', '早餐种类不丰富', '2014-07-30 11:11:22', '端木小红');
INSERT INTO `Proposal` VALUES ('4', '1234567', '早餐种类不丰富', '早餐种类不丰富', '2014-07-30 11:11:22', '端木小红');

-- ----------------------------
-- Table structure for VIP_Card_Get_Info
-- ----------------------------
DROP TABLE IF EXISTS `VIP_Card_Get_Info`;
CREATE TABLE `VIP_Card_Get_Info` (
  `nRecordId` int(20) NOT NULL AUTO_INCREMENT,
  `nAccountId` int(11) DEFAULT NULL,
  `nReceiveNum` int(11) DEFAULT NULL,
  `strCardDescription` varchar(20) DEFAULT NULL,
  `dtCollectionTime` datetime DEFAULT NULL,
  `strHandled` varchar(20) DEFAULT NULL,
  `strOperator` varchar(20) DEFAULT NULL,
  `dtOperatingTime` datetime DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_merchant2receive_info2` (`nAccountId`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of VIP_Card_Get_Info
-- ----------------------------
INSERT INTO `VIP_Card_Get_Info` VALUES ('1', '1234567', '1', '餐饮类卡号', '2014-07-30 11:24:55', '欧阳小明', '小红', '2014-07-30 11:25:17');
INSERT INTO `VIP_Card_Get_Info` VALUES ('2', '1234567', '1', '餐饮类卡号', '2014-07-30 11:24:55', '欧阳小明', '小红', '2014-07-30 11:25:17');
INSERT INTO `VIP_Card_Get_Info` VALUES ('3', '1234567', '1', '餐饮类卡号', '2014-07-30 11:24:55', '欧阳小明', '小红', '2014-07-30 11:25:17');
INSERT INTO `VIP_Card_Get_Info` VALUES ('4', '1234567', '1', '餐饮类卡号', '2014-07-30 11:24:55', '欧阳小明', '小红', '2014-07-30 11:25:17');
INSERT INTO `VIP_Card_Get_Info` VALUES ('5', '1234567', '1', '餐饮类卡号', '2014-07-30 11:24:55', '欧阳小明', '小红', '2014-07-30 11:25:17');

-- ----------------------------
-- Table structure for VIP_Consumption_History
-- ----------------------------
DROP TABLE IF EXISTS `VIP_Consumption_History`;
CREATE TABLE `VIP_Consumption_History` (
  `nRecordId` int(11) NOT NULL AUTO_INCREMENT,
  `nMembershipId` int(11) DEFAULT NULL,
  `VIP_nMembershipId` int(11) DEFAULT NULL,
  `nAccountId` int(11) DEFAULT NULL,
  `strCostType` char(20) DEFAULT NULL,
  `dCostAmount` char(20) DEFAULT NULL,
  `dtTime` char(20) DEFAULT NULL,
  `strOperatePerson` char(20) DEFAULT NULL,
  `nGetPoints` int(11) DEFAULT NULL,
  `bSettlement` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_VIP_many_consumption_history` (`VIP_nMembershipId`),
  KEY `FK_merchan2histories` (`nAccountId`),
  KEY `FK_vip2many_cost_history` (`nMembershipId`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of VIP_Consumption_History
-- ----------------------------
INSERT INTO `VIP_Consumption_History` VALUES ('1', '10001', '10001', '1234567', '现金消费', '1000', '2014-07-30 11:24:55', '小明', '1000', '1');
INSERT INTO `VIP_Consumption_History` VALUES ('2', '10001', '10001', '1234567', '现金消费', '1000', '07-30 11:24:55', '小明', '1000', '1');
INSERT INTO `VIP_Consumption_History` VALUES ('3', '10001', '10001', '1234567', '现金消费', '1000', '07-30 11:24:55', '小明', '1000', '1');
INSERT INTO `VIP_Consumption_History` VALUES ('4', '10001', '10001', '1234567', '现金消费', '1000', '07-30 11:24:55', '小明', '1000', '1');
INSERT INTO `VIP_Consumption_History` VALUES ('5', '10001', '10001', '1234567', '现金消费', '1000', '07-30 11:24:55', '小明', '1000', '1');
INSERT INTO `VIP_Consumption_History` VALUES ('6', '10001', '10001', '1234567', '现金消费', '1000', '07-30 11:24:55', '小明', '1000', '1');
INSERT INTO `VIP_Consumption_History` VALUES ('7', '10001', '10001', '1234567', '现金消费', '1000', '07-30 11:24:55', '小明', '1000', '1');
INSERT INTO `VIP_Consumption_History` VALUES ('8', '10001', '10001', '1234567', '现金消费', '1000', '07-30 11:24:55', '小明', '1000', '1');
INSERT INTO `VIP_Consumption_History` VALUES ('9', '10001', '10001', '1234567', '现金消费', '1000', '07-30 11:24:55', '小明', '1000', '1');
INSERT INTO `VIP_Consumption_History` VALUES ('10', '10001', '10001', '1234567', '现金消费', '1000', '07-30 11:24:55', '小明', '1000', '1');

-- ----------------------------
-- Table structure for VIP_Disable
-- ----------------------------
DROP TABLE IF EXISTS `VIP_Disable`;
CREATE TABLE `VIP_Disable` (
  `nRecordId` int(20) NOT NULL AUTO_INCREMENT,
  `nMembershipId` int(11) DEFAULT NULL,
  `bStatus` tinyint(1) DEFAULT NULL,
  `strOperator` varchar(20) DEFAULT NULL,
  `dtOperateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_vip_stopinfo2` (`nMembershipId`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of VIP_Disable
-- ----------------------------
INSERT INTO `VIP_Disable` VALUES ('1', '10001', '0', '小青', '2014-07-25 11:37:45');
INSERT INTO `VIP_Disable` VALUES ('2', '10001', '0', '小青', '2014-07-25 11:37:45');
INSERT INTO `VIP_Disable` VALUES ('3', '10001', '0', '小青', '2014-07-25 11:37:45');
INSERT INTO `VIP_Disable` VALUES ('4', '10001', '0', '小青', '2014-07-25 11:37:45');
INSERT INTO `VIP_Disable` VALUES ('5', '10001', '0', '小青', '2014-07-25 11:37:45');
INSERT INTO `VIP_Disable` VALUES ('6', '10001', '0', '小青', '2014-07-25 11:37:45');
INSERT INTO `VIP_Disable` VALUES ('7', '10001', '0', '小青', '2014-07-25 11:37:45');
INSERT INTO `VIP_Disable` VALUES ('8', '10001', '0', '小青', '2014-07-25 11:37:45');
INSERT INTO `VIP_Disable` VALUES ('9', '10001', '0', '小青', '2014-07-25 11:37:45');
INSERT INTO `VIP_Disable` VALUES ('10', '10001', '0', '小青', '2014-07-25 11:37:45');

-- ----------------------------
-- Table structure for VIP_Info
-- ----------------------------
DROP TABLE IF EXISTS `VIP_Info`;
CREATE TABLE `VIP_Info` (
  `nMembershipId` int(11) NOT NULL AUTO_INCREMENT,
  `strMembershipCardId` varchar(20) DEFAULT NULL,
  `nRecordId` int(20) DEFAULT NULL,
  `strPapersTypeChange` varchar(20) DEFAULT NULL,
  `strPapersId` varchar(20) DEFAULT NULL,
  `strMenbershipName` varchar(20) DEFAULT NULL,
  `strSex` varchar(20) DEFAULT NULL,
  `strCellphone` varchar(11) DEFAULT NULL,
  `strMenbershipCardType` varchar(20) DEFAULT NULL,
  `strMenbershipAdress` varchar(20) DEFAULT NULL,
  `strEmail` varchar(20) DEFAULT NULL,
  `nCardSendMerchantId` int(11) DEFAULT NULL,
  `strOperator` varchar(20) DEFAULT NULL,
  `dtOperateTime` datetime DEFAULT NULL,
  `strName` char(20) DEFAULT NULL,
  `strPasswd` char(20) DEFAULT NULL,
  `strVerificationCode` char(20) DEFAULT NULL,
  `strTradingPasswd` char(20) DEFAULT NULL,
  `nTotalPoints` int(11) DEFAULT NULL,
  PRIMARY KEY (`nMembershipId`),
  KEY `FK_card_to_vip` (`strMembershipCardId`),
  KEY `FK_vip_stopinfo` (`nRecordId`)
) ENGINE=MyISAM AUTO_INCREMENT=10011 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of VIP_Info
-- ----------------------------
INSERT INTO `VIP_Info` VALUES ('10001', '1122334', '1', '身份证', '276678201412329876', '南宫烧饼', '男', '16978546666', '实物会员卡', '四川省成都市高新西区888号', '66666@163.com', '1', '太史小明', '2014-08-01 13:11:56', '1234567', 'root', 'E8IJ', 'a123456', '10000');

-- ----------------------------
-- Table structure for VIP_Info_Change_History
-- ----------------------------
DROP TABLE IF EXISTS `VIP_Info_Change_History`;
CREATE TABLE `VIP_Info_Change_History` (
  `nRecordId` int(11) NOT NULL AUTO_INCREMENT,
  `nMembershipId` int(11) DEFAULT NULL,
  `strCardChangeType` varchar(20) DEFAULT NULL,
  `strPapersId` varchar(20) DEFAULT NULL,
  `strMenbershipName` varchar(20) DEFAULT NULL,
  `strSex` varchar(20) DEFAULT NULL,
  `strCellphone` varchar(11) DEFAULT NULL,
  `dtBrithday` datetime DEFAULT NULL,
  `strMenbershipAdress` varchar(20) DEFAULT NULL,
  `strEmail` varchar(20) DEFAULT NULL,
  `strMerchantChange` varchar(20) DEFAULT NULL,
  `strChangeReason` text,
  `strOperator` varchar(20) DEFAULT NULL,
  `dtOperateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `AK_Identifier_1` (`strCardChangeType`),
  KEY `FK_many_changes_to_vip` (`nMembershipId`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of VIP_Info_Change_History
-- ----------------------------
INSERT INTO `VIP_Info_Change_History` VALUES ('1', '10001', '身份证', '276678201412329876', '南宫烧饼', '男', '16978546666', '2014-07-25 13:14:48', '四川省成都市高新西区888号', '66666@163.com', '2', '需要购买此商家产品', '上官小明', '2014-07-26 13:17:22');
INSERT INTO `VIP_Info_Change_History` VALUES ('2', '10001', '身份证', '276678201412329876', '南宫烧饼', '男', '16978546666', '2014-07-25 13:14:48', '四川省成都市高新西区888号', '66666@163.com', '2', '需要购买此商家产品', '上官小明', '2014-07-26 13:17:22');
INSERT INTO `VIP_Info_Change_History` VALUES ('3', '10001', '身份证', '276678201412329876', '南宫烧饼', '男', '16978546666', '2014-07-25 13:14:48', '四川省成都市高新西区888号', '66666@163.com', '2', '需要购买此商家产品', '上官小明', '2014-07-26 13:17:22');
INSERT INTO `VIP_Info_Change_History` VALUES ('4', '10001', '身份证', '276678201412329876', '南宫烧饼', '男', '16978546666', '2014-07-25 13:14:48', '四川省成都市高新西区888号', '66666@163.com', '2', '需要购买此商家产品', '上官小明', '2014-07-26 13:17:22');
INSERT INTO `VIP_Info_Change_History` VALUES ('5', '10001', '身份证', '276678201412329876', '南宫烧饼', '男', '16978546666', '2014-07-25 13:14:48', '四川省成都市高新西区888号', '66666@163.com', '2', '需要购买此商家产品', '上官小明', '2014-07-26 13:17:22');
INSERT INTO `VIP_Info_Change_History` VALUES ('6', '10001', '身份证', '276678201412329876', '南宫烧饼', '男', '16978546666', '2014-07-25 13:14:48', '四川省成都市高新西区888号', '66666@163.com', '2', '需要购买此商家产品', '上官小明', '2014-07-26 13:17:22');
INSERT INTO `VIP_Info_Change_History` VALUES ('7', '10001', '身份证', '276678201412329876', '南宫烧饼', '男', '16978546666', '2014-07-25 13:14:48', '四川省成都市高新西区888号', '66666@163.com', '2', '需要购买此商家产品', '上官小明', '2014-07-26 13:17:22');
INSERT INTO `VIP_Info_Change_History` VALUES ('8', '10001', '身份证', '276678201412329876', '南宫烧饼', '男', '16978546666', '2014-07-25 13:14:48', '四川省成都市高新西区888号', '66666@163.com', '2', '需要购买此商家产品', '上官小明', '2014-07-26 13:17:22');
INSERT INTO `VIP_Info_Change_History` VALUES ('9', '10001', '身份证', '276678201412329876', '南宫烧饼', '男', '16978546666', '2014-07-25 13:14:48', '四川省成都市高新西区888号', '66666@163.com', '2', '需要购买此商家产品', '上官小明', '2014-07-26 13:17:22');
INSERT INTO `VIP_Info_Change_History` VALUES ('10', '10001', '身份证', '276678201412329876', '南宫烧饼', '男', '16978546666', '2014-07-25 13:14:48', '四川省成都市高新西区888号', '66666@163.com', '2', '需要购买此商家产品', '上官小明', '2014-07-26 13:17:22');

-- ----------------------------
-- Table structure for Vip_Merchant_Mapping
-- ----------------------------
DROP TABLE IF EXISTS `Vip_Merchant_Mapping`;
CREATE TABLE `Vip_Merchant_Mapping` (
  `nRecordId` int(20) NOT NULL AUTO_INCREMENT,
  `nAccountId` int(11) DEFAULT NULL,
  `nMembershipId` int(11) DEFAULT NULL,
  `nPoints` int(11) DEFAULT NULL,
  `dBalance` decimal(20,0) DEFAULT NULL,
  `bMessage` char(20) DEFAULT NULL,
  `bUnCardConsumption` tinyint(1) DEFAULT NULL,
  `strTicketInfo` text,
  `strDefaultPayWay` char(20) DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_merchant2middle` (`nMembershipId`),
  KEY `FK_merchant2midle` (`nAccountId`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Vip_Merchant_Mapping
-- ----------------------------
INSERT INTO `Vip_Merchant_Mapping` VALUES ('1', '1234567', '10001', '10000', '7000', '1', '1', '哈哈哈哈哈哈哈', '信用卡刷卡');
INSERT INTO `Vip_Merchant_Mapping` VALUES ('2', '1234567', '10001', '10000', '7000', '1', '1', '哈哈哈哈哈哈哈', '信用卡刷卡');
INSERT INTO `Vip_Merchant_Mapping` VALUES ('3', '1234567', '10001', '10000', '7000', '1', '1', '哈哈哈哈哈哈哈', '信用卡刷卡');
INSERT INTO `Vip_Merchant_Mapping` VALUES ('4', '1234567', '10001', '10000', '7000', '1', '1', '哈哈哈哈哈哈哈', '信用卡刷卡');
INSERT INTO `Vip_Merchant_Mapping` VALUES ('5', '1234567', '10001', '10000', '7000', '1', '1', '哈哈哈哈哈哈哈', '信用卡刷卡');
INSERT INTO `Vip_Merchant_Mapping` VALUES ('6', '1234567', '10001', '10000', '7000', '1', '1', '哈哈哈哈哈哈哈', '信用卡刷卡');
INSERT INTO `Vip_Merchant_Mapping` VALUES ('7', '1234567', '10001', '10000', '7000', '1', '1', '哈哈哈哈哈哈哈', '信用卡刷卡');
INSERT INTO `Vip_Merchant_Mapping` VALUES ('8', '1234567', '10001', '10000', '7000', '1', '1', '哈哈哈哈哈哈哈', '信用卡刷卡');
INSERT INTO `Vip_Merchant_Mapping` VALUES ('9', '1234567', '10001', '10000', '7000', '1', '1', '哈哈哈哈哈哈哈', '信用卡刷卡');
INSERT INTO `Vip_Merchant_Mapping` VALUES ('10', '1234567', '10001', '10000', '7000', '1', '1', '哈哈哈哈哈哈哈', '信用卡刷卡');

-- ----------------------------
-- Table structure for VIP_Points_History
-- ----------------------------
DROP TABLE IF EXISTS `VIP_Points_History`;
CREATE TABLE `VIP_Points_History` (
  `nRecordId` int(20) NOT NULL AUTO_INCREMENT,
  `nMembershipId` int(11) DEFAULT NULL,
  `nAccountId` int(11) DEFAULT NULL,
  `dtChangeTimeScoreCheck` datetime DEFAULT NULL,
  `strAmountOfScoreChange` varchar(20) DEFAULT NULL,
  `strChangeReason` text,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_merchant2many_points_change` (`nAccountId`),
  KEY `FK_vip2many_history` (`nMembershipId`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of VIP_Points_History
-- ----------------------------
INSERT INTO `VIP_Points_History` VALUES ('1', '10001', '1234567', '2014-07-31 13:21:22', '2000', '消费商品');
INSERT INTO `VIP_Points_History` VALUES ('2', '10001', '1234567', '2014-07-31 13:21:22', '2000', '消费商品');
INSERT INTO `VIP_Points_History` VALUES ('3', '10001', '1234567', '2014-07-31 13:21:22', '2000', '消费商品');
INSERT INTO `VIP_Points_History` VALUES ('4', '10001', '1234567', '2014-07-31 13:21:22', '2000', '消费商品');
INSERT INTO `VIP_Points_History` VALUES ('5', '10001', '1234567', '2014-07-31 13:21:22', '2000', '消费商品');
INSERT INTO `VIP_Points_History` VALUES ('6', '10001', '1234567', '2014-07-31 13:21:22', '2000', '消费商品');
INSERT INTO `VIP_Points_History` VALUES ('7', '10001', '1234567', '2014-07-31 13:21:22', '2000', '消费商品');
INSERT INTO `VIP_Points_History` VALUES ('8', '10001', '1234567', '2014-07-31 13:21:22', '2000', '消费商品');
INSERT INTO `VIP_Points_History` VALUES ('9', '10001', '1234567', '2014-07-31 13:21:22', '2000', '消费商品');
INSERT INTO `VIP_Points_History` VALUES ('10', '10001', '1234567', '2014-07-31 13:21:22', '2000', '消费商品');

-- ----------------------------
-- Table structure for VIP_Requirements
-- ----------------------------
DROP TABLE IF EXISTS `VIP_Requirements`;
CREATE TABLE `VIP_Requirements` (
  `nRecordId` int(11) NOT NULL AUTO_INCREMENT,
  `nMembershipId` int(11) DEFAULT NULL,
  `strVIPRequirement` text,
  `dtPublicTime` datetime DEFAULT NULL,
  PRIMARY KEY (`nRecordId`),
  KEY `FK_vip_need` (`nMembershipId`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of VIP_Requirements
-- ----------------------------
INSERT INTO `VIP_Requirements` VALUES ('1', '10001', '双人房一间', '2014-07-30 13:22:36');
INSERT INTO `VIP_Requirements` VALUES ('2', '10001', '双人房一间', '2014-07-30 13:22:36');
INSERT INTO `VIP_Requirements` VALUES ('3', '10001', '双人房一间', '2014-07-30 13:22:36');
INSERT INTO `VIP_Requirements` VALUES ('4', '10001', '双人房一间', '2014-07-30 13:22:36');
INSERT INTO `VIP_Requirements` VALUES ('5', '10001', '双人房一间', '2014-07-30 13:22:36');
INSERT INTO `VIP_Requirements` VALUES ('6', '10001', '双人房一间', '2014-07-30 13:22:36');
INSERT INTO `VIP_Requirements` VALUES ('7', '10001', '双人房一间', '2014-07-30 13:22:36');
INSERT INTO `VIP_Requirements` VALUES ('8', '10001', '双人房一间', '2014-07-30 13:22:36');
INSERT INTO `VIP_Requirements` VALUES ('9', '10001', '双人房一间', '2014-07-30 13:22:36');
INSERT INTO `VIP_Requirements` VALUES ('10', '10001', '双人房一间', '2014-07-30 13:22:36');
