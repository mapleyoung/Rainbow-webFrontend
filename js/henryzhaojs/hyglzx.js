/**
*公共函数，空值，网络连接等
*
*/
var db;
var item;
function NotifyEmptyResult() {
	alert("结果为空！");
}
function NotifyTryAgain() {
	//alert("网络出错，请稍后再试！");
}
function initData(){
	
}
/**
*公共请求函数
*/
function ajaxAbstract(method,remote_url,send_data,parser_callback,dom_callback){
	$.ajax({
		type: method,
		url: remote_url,
		dataType: 'json',
		data:send_data,
		success: function(result) {
			if (result == "" || result == null) {
				NotifyEmptyResult();
			} else {
				dom_callback(parser_callback(result));
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			NotifyTryAgain();
		}
	});
}

function ajaxAbstractJson(method,remote_url,send_data,parser_callback){
	  
    $.ajax({
        type: "get",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
        url: remote_url,//要访问的后台地址
        data: send_data,
        complete :function(){},//AJAX请求完成时隐藏loading提示
        success: function(msg){        //msg为返回的数据，在这里做数据绑定
        	db=msg;
        	if (msg == "" || msg == null) {
				NotifyEmptyResult();
			} else {
				parser_callback(msg);
	           
	            //$("[@id=child_Account_table_template]").show();
	            		
    		}
    },
        error: function(xhr, textStatus, errorThrown) {
			//NotifyTryAgain();
		}
    });

}

	//修改会员基本信息
function personalCount(){

	var method="post";
	var URL="vIP_updateInfo.action";
	

	//获取表单数据

	/*var formData="vipInfo.strPapersTypeChange="+$("#strPapersTypeChange").val()
                +"&vipInfo.strPapersId="+$("#strPapersId").val()
                +"&vipInfo.strSex="+$("input[name='sexradio']:checked").val()
                +"&vipInfo.strCellphone="+$("#strCellphone").val()
	            +"&vipInfo.strMenbershipAdress="+$("#province").val()+$("#city").val()+$("#district").val()
	            +"&vipInfo.strEmail="+$("#strEmail").val()
	            +"&vipInfo.strPasswd="+$("#strPasswd").val()
	            +"&vipInfo.strTradingPasswd="+$("#strTradingPasswd").val()
	            +"&vipInfo.nmembershipId=10001";
                alert(formData);
	ajaxAbstract(method,URL,formData,parseJson,DomInsert);
	*/
	alert("恭喜你，信息修改成功！");


}
function parsedatetime(datetime){

	var tempdatetime=datetime;
	var resultdatetime=tempdatetime.year+1900+"-"+tempdatetime.month+"-"+tempdatetime.day+" "+tempdatetime.hours+":"+tempdatetime.minutes+":"+tempdatetime.seconds;
	return resultdatetime;
}
//个人积分历史
function person_points_history_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        var row = $("#template_Points").clone();//克隆一行出来
		row.find("#points_history_table_dtChangeTimeScoreCheck").text(parsedatetime(n.dtChangeTimeScoreCheck));
 		row.find("#points_history_table_points_before").text(5000-n.strAmountOfScoreChange); 	//该值表中不存在，需要计算后重新组装	
        row.find("#points_history_table_strAmountOfScoreChange").text(n.strAmountOfScoreChange);
        row.find("#points_history_table_strChangeReason").text(n.strChangeReason);
        row.find("#points_history_table_merchant_name").text("桂祥大酒店");//该值表中不存在，需要从其他表中获取再组装
        row.find("#points_history_table_points_after").text(n.strAmountOfScoreChange);//该值表中不存在，需要计算后重新组装
        row.attr("id","ready");//改变绑定好数据的行的id
        row.appendTo("#table_pointshistory");//添加到模板的容器中
    });
}
function person_pointshistory_query()
 {
 	var method="GET";
 	var remote_url="vIP_listPoints.action?vipId=10001&pageNo=1&pageSize=10";
	ajaxAbstractJson(method,remote_url,"",person_points_history_parseQueryJson);
}
//礼品兑换
function points2present(){

	var method="post";
	var URL="vIP_points2present.action";
	//获取表单数据
	var formData="points2present.strWayOfReceive="+$("#PointsToPresent_strWayOfReceive").val()
                +"&points2present.strMenbershipAdress="+$("#province1").val()+$("#city1").val()+$("#district1").val()
                alert("恭喜您礼品兑换方式选择成功！");
	ajaxAbstract(method,URL,formData,parseJson,DomInsert);


}
//积分提现
function money(){

	var method="post";
	var URL="vIP_points2Cash.action";
	//获取表单数据
	var formData="poinToCash.nCostPoints="+$("#PointsToCash_nCostPoints").val()
                +"&poinToCash.dAmountOfCash="+$("#PointsToCash_dAmountOfCash").val()
                +"&poinToCash.strWayOfReceive="+$("#PointsToCash_strWayOfReceive").val()
                +"&poinToCash.strBankAccount="+$("#PointsToCash_strBankAccount").val();
                alert("恭喜您！积分提现成功！~~");
	ajaxAbstract(method,URL,formData,parseJson,DomInsert);


}
//个人消费记录
function cost_history_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        var row = $("#template_costhistory").clone();//克隆一行出来
		row.find("#costhistory_table_nrecordId").text(n.nrecordId);
 		row.find("#costhistory_table_strCostType").text(n.strCostType); 		
        row.find("#costhistory_table_dCostAmount").text(n.dcostAmount);
        row.find("#costhistory_table_dtTime").text(n.dtTime);
        row.find("#costhistory_table_strOperatePerson").text(n.strOperatePerson);
        row.find("#costhistory_table_nGetPoints").text(n.ngetPoints);
        row.attr("id","ready");//改变绑定好数据的行的id
        row.appendTo("#table_costhistory");//添加到模板的容器中
    });
}
function costhistory_query()
 {
 	var method="GET";
 	var remote_url="vIP_listCostHistory.action?vipId=10001&pageNo=1&pageSize=10";
	ajaxAbstractJson(method,remote_url,"",cost_history_parseQueryJson);
}
//dom插入
function DomInsert(str)
{
	var $boxes = $(str);
	//var $newInsert = $('#sample').html( $boxes );
	showDlg(infoDlgId,infoTextId,str);
}
//格式化数据
function parseJson(data)
{
	//var html="";
	//$.each(data.res,function(i,item){
	//	html=html+"<tr><td>"+item.name+"</td><td>"+item.percetage+"</td></tr>"
	//});
	return data.state;
}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	$('#btn_vipinfo').click(function(){personalCount();});
	$('#btn_p2p').click(function(){points2present();});
	$('#btn_p2c').click(function(){money();});
	//$('#list_pointshistory').click(function(){person_pointshistory_query();});
	//$('#list_costrecord').click(function(){costhistory_query();});
});

