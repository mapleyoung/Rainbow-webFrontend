
        $(document).ready(function () {

            //zlz:试一下
            var urlinfo = window.location.href; //获取当前页面的url 
            var len = urlinfo.length;//获取url的长度 
            var offset = urlinfo.indexOf("?");//设置参数字符串开始的位置 
            var newsidinfo = urlinfo.substr(offset, len)//取出参数字符串 这里会获得类似“id=1”这样的字符串 
            var newsids = newsidinfo.split("=");//对获得的参数字符串按照“=”进行分割 
            var shop_Id = newsids[1];

            //测试id是否获取到
           // document.getElementById("smMiddleName").textContent=newsid;


            $.ajax({
               url: "merchant_showProduct.action", //对应action的路径
               dataType: "json",
               cache: false,
               data: { merchantId: shop_Id ,pageNo:1,pageSize:20},   //传给程英的商家id
               type: "POST",
               success: function (data) {

                    var obj = data;

                    //商家详细信息：地址、简介、路线等等
                    var shopname = obj[0].merchantName;
                    var shopaddress = obj[0].merchantAddr;
                    var shopdescribe = obj[0].merchantIntr;

                    var showName = document.getElementById("showConMerName");
                    var showAddress = document.getElementById("showConAddrActual");
                    var showDescribe = document.getElementById("showConBriefContent");

                    showName.textContent = shopname;
                    showAddress.textContent = shopaddress;
                    showDescribe.textContent = shopdescribe;

                    //商家信息、logo
                    var shop_src = obj[0].merchantImg;
                    var shop_logo = obj[0].merchantLogo;

                    var shopLogo = document.getElementById("smBrandSmall");
                    var shopName = document.getElementById("smMiddleName");
                    var shopImg = document.getElementById("smBigBack");

                    shopLogo.src = shop_logo;
                    shopName.textContent = shopname;
                    shopImg.src = shop_src;

                    //商家对应的产品展示
                    for (var i = 1; i < obj.length; i++) {
                        var src = obj[i].productPic;  //传回来的对应的属性名称
                        var productName = obj[i].productName; //传回来的对应的属性名称
                        var price = obj[i].productPrice; //传回来的对应的属性名称
                        var productId = obj[i].productId;//传回来的对应的属性名称

                        var num = i;
                        var img_id = "img" + num;
                        var p1_id = "p" + num + "1";
                        var s_id = "s" + num;
                        var a_id = "a" + num;

                        var img = document.getElementById(img_id);
                        var p1 = document.getElementById(p1_id);
                        var s = document.getElementById(s_id);
                        var a = document.getElementById(a_id);

                        img.style.backgroundImage = "url(" + src + ")";
                        p1.textContent = productName;
                        s.textContent = "￥" + price;
                        a.href = "showProduct.html?id=" + productId + "&src=" + shop_logo;
                    }
                    if (obj.length < 9) {
                        for (var i = obj.length; i <= 8; i++) {


                            var num = i + 1;
                            var img_id = "img" + num;
                            var p1_id = "p" + num + "1";

                            var img = document.getElementById(img_id);
                            var p1 = document.getElementById(p1_id);

                            img.style.backgroundImage = "url(img/picture/qidai.jpg)";
                            p1.textContent = "暂无";
                        }
                    }
                }
            });
        });