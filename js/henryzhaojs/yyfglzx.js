/**
*公共函数，空值，网络连接等
*
*/
var db;
var item;
function NotifyEmptyResult() {
	alert("结果为空！");
}
function NotifyTryAgain() {
	//alert("网络出错，请稍后再试！");
}
function initData(){
	
}
/**
*公共请求函数
*/
//查找某一条数据的详细信息
function findOne(db,constr,conval){
	var object;
	$.each(db,
	function(i, item) {
		if (item[constr]==conval) {
			object = item;
			return false;
		}
	});
	return object;
}
function showDlg(Dlgid,textId,msg){
	$(textId).html(msg);
	$(Dlgid).modal("show");
}
function parsedatetime(datetime){

	var tempdatetime=datetime;
	var resultdatetime=tempdatetime.year+1900+"-"+tempdatetime.month+"-"+tempdatetime.day+" "+tempdatetime.hours+":"+tempdatetime.minutes+":"+tempdatetime.seconds;
	return resultdatetime;
}
function ajaxAbstract(method,remote_url,send_data,parser_callback,dom_callback){
	$.ajax({
		type: method,
		url: remote_url,
		dataType: 'json',
		data:send_data,
		success: function(result) {
			if (result == "" || result == null) {
				NotifyEmptyResult();
			} else {
				dom_callback(parser_callback(result));
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			NotifyTryAgain();
		}
	});
}

function ajaxAbstractJson(method,remote_url,send_data,parser_callback){
	  
    $.ajax({
        type: "get",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
        url: remote_url,//要访问的后台地址
        data: send_data,
        complete :function(){},//AJAX请求完成时隐藏loading提示
        success: function(msg){        //msg为返回的数据，在这里做数据绑定
        	db=msg;
        	if (msg == "" || msg == null) {
				NotifyEmptyResult();
			} else {
				parser_callback(msg);
	           
	            //$("[@id=child_Account_table_template]").show();
	            		
    		}
    },
        error: function(xhr, textStatus, errorThrown) {
			//NotifyTryAgain();
		}
    });

}


//商家入盟申请列表
function merchant_apply_showdialog(nRecordId) {
	//var person=findOne(db,"nRecordId",nRecordId);
	//if (person == null)
	 //return null;
	
	$("#myModal_shenqing_xiangqing").modal("show");
}
function merchant_apply_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
	 	var tempState;
        var row = $("#template_merchant_apply").clone();//克隆一行出来
		row.find("#merchant_apply_table_id").text(n.strMerchantId);
 		row.find("#merchant_apply_table_merchantname").text(n.strMerchantName); 	//该值表中不存在，需要从其他表中获取后重新组装	
        if(n.nState==1){			//对账户类型进行判断
        	tempState="待处理";
        }else if(n.nState==2){
        	tempState="已通过";
 		}else if(n.nState==3){
 			tempState="未通过";
 		}else if(n.nState==4){
 			tempState="已分配";
 		}
        row.find("#merchant_apply_table_state").text(tempState);
        row.find("#merchant_apply_table_person").text(n.strOperator);
        row.find("#merchant_apply_table_time").text(n.strTime);
        row.find("#merchant_apply_table_operation").html("<button type='button' class='btn btn-primary' onclick='merchant_apply_showdialog("+n.strMerchantId+")' >编辑</button>&nbsp"+
                 		"<button type='button' class='btn btn-danger'>删除</button>");
        row.attr("id","ready");//改变绑定好数据的行的id
        row.appendTo("#table_merchant_apply");//添加到模板的容器中
    });
}
function merchant_apply_query()
 {
 	var method="GET";

 	if($("#select_apply_state").val()=="0"){
 		var remote_url="manager_listReg.action?state=1&pageNo=1&pageSize=10";//没和action对应 state=1
 	}else if($("#select_apply_state").val()=="1"){
 		var remote_url="manager_listReg.action?state=2&pageNo=1&pageSize=10";//没和action对应 state=2
 	}else if($("#select_apply_state").val()=="2"){
 		var remote_url="manager_listReg.action?state=3&pageNo=1&pageSize=10";//没和action对应 state=3
 	}else if($("#select_apply_state").val()=="3"){
 		var remote_url="manager_listReg.action?state=4&pageNo=1&pageSize=10";//没和action对应 state=4
 	}
 	ajaxAbstractJson(method,remote_url,"",merchant_apply_parseQueryJson);
}
//商家管理记录
function merchant_manage_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        var row = $("#template_merchant_manage").clone();//克隆一行出来
		row.find("#merchant_manage_table_merchant_account").text(n.naccountId);   //在Merchant_Account_Info表
 		row.find("#merchant_manage_table_merchant_name").text(n.strName); 	//在Merchant_Reg_Info表
        row.find("#merchant_manage_table_merchant_addr").text(n.strAddress);   //在Merchant_Reg_Info表
        row.find("#merchant_manage_table_merchant_phone").text(n.strName);//在Merchant_Reg_Info表
        row.find("#merchant_manage_table_merchant_operation").html("<button type='button' class='btn btn-info' onclick='showdialog_yzk("+n.naccountId+")'>编辑</button>&nbsp" +
	                 		"<button type='button' class='btn btn-danger'>删除</button>")
        row.attr("id","ready");//改变绑定好数据的行的id
        row.appendTo("#table_merchant_manage");//添加到模板的容器中
    });
}
function merchant_manage_query()
 {
 	var method="GET";
 	var aaa=$("#if_recommended").val();
 	//alert(aaa);
 	if(aaa=="0"){
 		var remote_url="manager_findMerchant.action?bIsRecommend=0&pageNo=1&pageSize=10";//没和action对应  bIsRecommend=0
 	}else if(aaa=="1"){
 		var remote_url="manager_findMerchant.action?bIsRecommend=1&pageNo=1&pageSize=10";//没和action对应  bIsRecommend=1
 	}
	ajaxAbstractJson(method,remote_url,"",merchant_manage_parseQueryJson);
}
//展示信息审批列表
function audit_showinfo_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
	 	var tempState;
        var row = $("#template_product_info").clone();//克隆一行出来
		row.find("#product_info_id").text(n.nrecordId);
 		row.find("#product_info_merchantname").text(n.strMerchantName); 		
        if(n.nstate==1){			//对账户类型进行判断
        	tempState="未审批";
        }else if(n.nstate==2){
        	tempState="已通过";
 		}else if(n.nstate==3){
 			tempState="未通过";
 		}
        row.find("#product_info_state").text(tempState);
        row.find("#product_info_person").text(n.strOperator);
        row.find("#product_info_time").text(parsedatetime(n.dtTime));
        row.find("#product_info_operation").html("<button type='button' class='btn btn-info' onclick='showdialog_yzk("+n.nrecordId+")'>编辑</button>&nbsp" +
	                 		"<button type='button' class='btn btn-danger'>删除</button>");
        row.attr("id","ready");//改变绑定好数据的行的id
        row.appendTo("#table_showinfo");//添加到模板的容器中
    });
}
function audit_showinfo_query()
 {
 	var method="GET";

 	if($("#showinfo_state").val()=="0"){
 		var remote_url="manager_findMerchantShowMsg.action?state=1&pageNo=1&pageSize=10";//没和action对应 state=0
 	}else if($("#showinfo_state").val()=="1"){
 		var remote_url="manager_findMerchantShowMsg.action?state=2 &pageNo=1&pageSize=10";//没和action对应 state=1
 	}else if($("#showinfo_state").val()=="2"){
 		var remote_url="manager_findMerchantShowMsg.action?state=3&pageNo=1&pageSize=10";//没和action对应 state=2
 	}
 	ajaxAbstractJson(method,remote_url,"",audit_showinfo_parseQueryJson);
}
//会员卡领取登记  要修改
function myModal_register_card(){

	var method="post";
	var URL="vIP_points2Cash.action";//url错误
	//获取表单数据
	var formData="poinToCash.nAccountId="+$("#receive_merchant_account").val()
                +"&poinToCash.strMerchantName="+$("#receive_merchant_name").val()
                +"&vipCardGetInfo.nreceiveNum="+$("#receive_number").val()
                +"&vipCardGetInfo.strCardDescription="+$("#receive_pre_cardsection").val()
                +"&vipCardGetInfo.dtCollectionTime="+$("#receive_time").val()
                +"&vipCardGetInfo.strHandled="+$("#receive_receiptor").val()
                +"&vipCardGetInfo.strOperator="+$("#receive_operator").val();
                //alert(formData);
	ajaxAbstract(method,URL,formData,parseJson,DomInsert);


}
//会员卡领取历史列表
function get_card_history_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        var row = $("#template_getcard_history").clone();//克隆一行出来
		row.find("#getcard_history_merchant_account").text(n.nAccountId);//该值表中不存在，需要从其他表中获取再组装
 		row.find("#getcard_history_merchant_name").text(n.strMerchantName); //该值表中不存在，需要从其他表中获取再组装	
        row.find("#getcard_history_number").text(n.nreceiveNum);
        row.find("#getcard_history_cardsection").text(n.strCardDescription);
        row.find("#getcard_history_time").text(n.dtCollectionTime);
        row.find("#getcard_history_receipter").text(n.strHandled);
        row.find("#getcard_history_operator").text(n.strOperator);
        row.attr("id","ready");//改变绑定好数据的行的id
        row.appendTo("#table_get_card_history");//添加到模板的容器中
    });
}
function get_card_history_query()
 {
 	var method="GET";
 	var remote_url="vIP_listPoints.action?vipId=10001&pageNo=1&pageSize=10";
	ajaxAbstractJson(method,remote_url,"",get_card_history_parseQueryJson);
}
//积分消费列表
function points_history_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        var row = $("#template_points_history").clone();//克隆一行出来
		row.find("#points_history_time").text(parsedatetime(n.dtChangeTimeScoreCheck));
 		row.find("#points_history_first_points").text("5000"); //该值表中不存在，需要计算后重新组装	
        row.find("#points_history_cost_points").text(n.strAmountOfScoreChange);
        row.find("#points_history_cost_ways").text(n.strChangeReason);
        row.find("#points_history_shop").text("爱木马");//该值表中不存在，需要从其他表中获取再组装
        row.find("#points_history_operator").text("Aer");//该值表中不存在，需要从其他表中获取再组装
        row.find("#points_history_last_points").text(5000-n.nLastPoints);//该值表中不存在，需要计算后重新组装
        row.attr("id","ready");//改变绑定好数据的行的id
        row.appendTo("#table_points_history");//添加到模板的容器中
    });
}
function points_history_query()
 {
 	var method="GET";
 	var remote_url="vIP_listPoints.action?vipId=10001&pageNo=1&pageSize=10";
	ajaxAbstractJson(method,remote_url,"",points_history_parseQueryJson);
}
//商家基础信息维护列表
function merchant_info_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        var row = $("#template_merchant_info").clone();//克隆一行出来
		row.find("#merchant_info_merchant_id").text(n.nrecordId);
 		row.find("#merchant_info_merchant_name").text("桂祥酒店"); 	
        row.find("#merchant_info_merchant_type").text("酒店");
        row.find("#merchant_info_merchant_star").text("三星");
        row.find("#merchant_info_merchant_addr").text("四川成都");
        row.find("#merchant_info_merchant_phone").text("15972102336");
        row.find("#merchant_info_merchant_operation").html();
        row.attr("id","ready");//改变绑定好数据的行的id
        row.appendTo("#table_merchant_info");//添加到模板的容器中
    });
}
function merchant_info_query()
 {
 	var method="GET";
 	var remote_url="vIP_listPoints.action?vipId=10001&pageNo=1&pageSize=10";
	ajaxAbstractJson(method,remote_url,"",merchant_info_parseQueryJson);
}
//dom插入
function DomInsert(str)
{
	var $boxes = $(str);
	//var $newInsert = $('#sample').html( $boxes );
	showDlg(infoDlgId,infoTextId,str);
}
//格式化数据
function parseJson(data)
{
	//var html="";
	//$.each(data.res,function(i,item){
	//	html=html+"<tr><td>"+item.name+"</td><td>"+item.percetage+"</td></tr>"
	//});
	return data.state;
}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	$('#btn_receive_sub').click(function(){myModal_register_card();});
	$('#shenqing_list_Btn').click(function(){merchant_apply_query();});
	$('#sjsearch_list_Btn').click(function(){merchant_manage_query();});
	$('#dislayinfo_change_Btn').click(function(){audit_showinfo_query();});
	$('#list_receivehistory').click(function(){get_card_history_query();});
	$('#point_shopping_Btn').click(function(){points_history_query();});
	$('#sj_details_list_Btn').click(function(){merchant_info_query();});
});

function gifts_get_Btn_click(){
	 $("#gifts_get_Btn_click_div").fadeToggle(500);
}