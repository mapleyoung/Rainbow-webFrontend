/**
*全局变量
*
*/
var db;//子账号单独查询的数据
var item;
var merchantId;


/**
*公共函数，空值，网络连接等
*
*/
function NotifyEmptyResult() {
	alert("结果为空！");
}

function initData(){
	
}
/**
*公共提交函数
*return text
*/
function ajaxAbstractText(method,remote_url,send_data,parser_callback){
	$.ajax({
		type: method,
		url: remote_url,
		dataType: 'text',
		data:send_data,
		success: function(result) {
			if (result == "" || result == null) {
				NotifyEmptyResult();
			} else {
				parser_callback(result);
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			//NotifyTryAgain();
		}
	});
}
/**
*公共获取表单数据函数 
* return json
*/
function ajaxAbstractJson(method,remote_url,send_data,parser_callback){
	  
    $.ajax({
        type: "get",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
        url: remote_url,//要访问的后台地址
        data: send_data,
        complete :function(){},//AJAX请求完成时隐藏loading提示
        success: function(msg){        //msg为返回的数据，在这里做数据绑定
        	db=msg;
        	if (msg == "" || msg == null) {
				NotifyEmptyResult();
			} else {
				parser_callback(msg);
	           
	            //$("[@id=child_Account_table_template]").show();
	            		
    		}
    },
        error: function(xhr, textStatus, errorThrown) {
			//NotifyTryAgain();
		}
    });

}


/**
*sjglzx_addChild
*添加子账户
* 并不需要添加子账户的弹框
*/
/*================================================henryzhao:添加子账号  start=================================================================*/
function addChildAccount(){

	//var formId="person_apply_form_id";
	//var infoDlgId="#DlgResult";
	//var infoTextId="#DlgResult_Body";
	var method="GET";
	var URL="merchant_addChild.action";
	

	var temp;
	if ($("#bInUse").val()=="on") {
		temp=0;
	}else if($("#bInUse").val()=="off"){
		temp=1;
	};
	//获取表单数据
	var formData="merchant.naccountType="+$("#zzhlx").val()
                +"&merchant.strName="+$("#zzh").val()
                +"&merchant.strPasswd="+$("#zzhmm").val()
                +"&merchant.bInUse="+temp
                +"&merchantId=1234567";
               
               // alert(formData);
	ajaxAbstractText(method,URL,formData,parseJson);


}
//dom插入
function DomInsert(str)
{
	var $boxes = $(str);
	//var $newInsert = $('#sample').html( $boxes );
	showDlg(infoDlgId,infoTextId,str);
}
//格式化数据
function parseJson(data)
{
	//var html="";
	//$.each(data.res,function(i,item){
	//	html=html+"<tr><td>"+item.name+"</td><td>"+item.percetage+"</td></tr>"
	//});
if (data=="success") {
	alert("恭喜你操作成功！");
}else{
	alert("真遗憾操作失败！");
};
}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	$('#qrAddSubcount_Btn').click(function(){addChildAccount();});
});
/*================================================henryzhao:添加子账号  End=================================================================*/
/*================================================henryzhao:子账号列表  start=================================================================*/
function showdialog_zzh(naccountId) {
	var person=findOne(db,"naccountId",naccountId);
	if (person == null)
	 return null;
	//$("naccountId").val("2014");
	//$("#Person_DlgDetail_Remarks").html("nothing");
	$("#myModal1zzhbj_edit_zzh").text(person.strName)
	//if(person.naccountType==0){$("#myModal1zzhbj_edit_zzhlx").text("哈哈")};
	$("#myModal1zzhbj").modal("show");
}
function findOne(db,constr,conval){
	var object;
	$.each(db,
	function(i, item) {
		if (item[constr]==conval) {
			object = item;
			return false;
		}
	});
	return object;
}
function deletedlg(){
	alert("确定要删除此数据？");
}
function parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
     	var tempType;
        	var row = $("#child_Account_table_template").clone();//克隆一行出来
			row.find("#child_Account_table_nAccountId").text(n.naccountId);//n.OrderID数据是应哥后台封装过来的数据的名字，n只是代号，OrderID才是需要对应的
			if(n.naccountType==0){			//对账户类型进行判断
 			tempType="前台";
 			}else if(n.naccountType==1){
 				tempType="管理员";
 			}else if(n.naccountType==2){
 				tempType="客户经理";
 			}
 		row.find("#child_Account_table_naccountType").text(tempType); 		
        row.find("#child_Account_table_strName").text(n.strName);
        row.find("#child_Account_table_strPasswd").text(n.strPasswd);
        row.find("#child_Account_table_bInUse").html( "<input name='checkbox' type='checkbox' disabled data-toggle='switch'/>");
        row.find("#child_Account_table_operate").html( "<button type='button' class='btn btn-primary' onclick='showdialog_zzh("+n.naccountId+")' >编辑</button>&nbsp"+
                 "<button type='button' class='btn btn-info' onclick='showdialog_zzh("+n.naccountId+")'>查看</button>&nbsp" +
                 		"<button type='button' class='btn btn-danger' onclick='deletedlg();'>删除</button>");                   
        row.attr("id","ready");//改变绑定好数据的行的id
        row.appendTo("#datas");//添加到模板的容器中
    });
}
function childAccount_query()
 {
 	var method="GET";
 	var remote_url="merchant_listChild.action?merchantId=1234567&pageNo=1&pageSize=10";
	//ajaxAbstract(method_query,URL_query,"",parseQueryJson,dom_handle_query);
	ajaxAbstractJson(method,remote_url,"",parseQueryJson);
}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	
	$('#AddSubcount_test_Btn').click(function(){
		childAccount_query();});
});
/*================================================henryzhao:子账号列表  End===================================================*/
/*================================================henryzhao:商家配置管理 start==================================================================*/
//IP绑定
function FrontIPBind(){
	var ip_state=$("#myModal1pzip_state").val();
	var ip_myModal1pzip=$("#myModal1pzip_ip").val();
	if (ip_state=="on") {
		$("#sjglpz_ip_state_div").empty();
		$("#sjglpz_ip_state_div").append("<div class='switch has-switch deactivate'><div class='switch-on'><input name='checkbox' checked type='checkbox' disabled data-toggle='switch' /><span class='switch-left'>ON</span><lable></lable><span class='switch-right'>OFF</span></div></div>");
	}else if(ip_state=="off"){
		$("#sjglpz_ip_state_div").empty();
		$("#sjglpz_ip_state_div").append("<div class='switch has-switch deactivate'><div class='switch-off'><input name='checkbox' checked type='checkbox' disabled data-toggle='switch' /><span class='switch-left'>ON</span><lable></lable><span class='switch-right'>OFF</span></div></div>");
	}
	$("#sjglpz_ip_value").text(ip_myModal1pzip);
	$("#myModal1pzip").modal('hide');

}
//权限设置
function myModalpzcz_fun(){
	var qtcz_state=$("#myModalpzcz_qtcz_id").val();
	var khjlcz_state=$("#myModalpzcz_khjlcz_id").val();
	if (qtcz_state=="on") {
		$("#qtcz_id_div").empty();
		$("#qtcz_id_div").append("<div class='switch has-switch deactivate'><div class='switch-on'><input name='checkbox' checked type='checkbox' disabled data-toggle='switch' /><span class='switch-left'>ON</span><lable></lable><span class='switch-right'>OFF</span></div></div>");
	}else if(qtcz_state=="off"){
		$("#qtcz_id_div").empty();
		$("#qtcz_id_div").append("<div class='switch has-switch deactivate'><div class='switch-off'><input name='checkbox' checked type='checkbox' disabled data-toggle='switch' /><span class='switch-left'>ON</span><lable></lable><span class='switch-right'>OFF</span></div></div>");
	}
	if (khjlcz_state=="on") {
		$("#khjlcz_id_div").empty();
		$("#khjlcz_id_div").append("<div class='switch has-switch deactivate'><div class='switch-on'><input name='checkbox' checked type='checkbox' disabled data-toggle='switch' /><span class='switch-left'>ON</span><lable></lable><span class='switch-right'>OFF</span></div></div>");
	}else if(khjlcz_state=="off"){
		$("#khjlcz_id_div").empty();
		$("#khjlcz_id_div").append("<div class='switch has-switch deactivate'><div class='switch-off'><input name='checkbox' checked type='checkbox' disabled data-toggle='switch' /><span class='switch-left'>ON</span><lable></lable><span class='switch-right'>OFF</span></div></div>");
	}
	
	$("#myModalpzcz").modal('hide');

}
//消费方式
function myModalpzxffs_fun(){
	var xffs_item=$("input[name='pzxffs_optionsRadios']:checked").val();
	
	$("#pzxffs_lable_id").empty();
	$("#pzxffs_lable_id").append("<h7>"+xffs_item+"</h7>");
	$("#myModalpzxffs").modal('hide');
}
//无卡添加接待记录设定
function myModalpzwkjd_fun(){
	var zhzhh_state=$("#myModalpzwkjd_zhzhh_id").val();
	var jlzhh_state=$("#myModalpzwkjd_jlzhh_id").val();
	if (zhzhh_state=="on") {
		$("#zhzhh_id_div").empty();
		$("#zhzhh_id_div").append("<div class='switch has-switch deactivate'><div class='switch-on'><input name='checkbox' checked type='checkbox' disabled data-toggle='switch' /><span class='switch-left'>ON</span><lable></lable><span class='switch-right'>OFF</span></div></div>");
	}else if(zhzhh_state=="off"){
		$("#zhzhh_id_div").empty();
		$("#zhzhh_id_div").append("<div class='switch has-switch deactivate'><div class='switch-off'><input name='checkbox' checked type='checkbox' disabled data-toggle='switch' /><span class='switch-left'>ON</span><lable></lable><span class='switch-right'>OFF</span></div></div>");
	}
	if (jlzhh_state=="on") {
		$("#jlzhh_id_div").empty();
		$("#jlzhh_id_div").append("<div class='switch has-switch deactivate'><div class='switch-on'><input name='checkbox' checked type='checkbox' disabled data-toggle='switch' /><span class='switch-left'>ON</span><lable></lable><span class='switch-right'>OFF</span></div></div>");
	}else if(jlzhh_statee=="off"){
		$("#jlzhh_id_div").empty();
		$("#jlzhh_id_div").append("<div class='switch has-switch deactivate'><div class='switch-off'><input name='checkbox' checked type='checkbox' disabled data-toggle='switch' /><span class='switch-left'>ON</span><lable></lable><span class='switch-right'>OFF</span></div></div>");
	}
	
	$("#myModalpzwkjd").modal('hide');

}
//打印小票
function myModalpzxpdy_fun(){
	var dyxp_item=$("input[name='print_optionsRadios']:checked").val();
	
	$("#xpdy_lable_id").empty();
	$("#xpdy_lable_id").append("<h7>"+dyxp_item+"</h7>");
	$("#myModalpzxpdy").modal('hide');
}
//短信重发
function myModalpzdxcf_fun(){
	var dxcf_item=$("input[name='dxcf_optionsRadios']:checked").val();
	
	$("#dxcf_lable_id").empty();
	$("#dxcf_lable_id").append("<h7>"+dxcf_item+"</h7>");
	$("#myModalpzdxcf").modal('hide');
}
/*================================================henryzhao:商家配置管理 end====================================================================*/

/*================================================henryzhao:商家信息维护  start=================================================================*/
function addMerchantInfo(){

	//var formId="person_apply_form_id";
	//var infoDlgId="#DlgResult";
	//var infoTextId="#DlgResult_Body";
	var method="GET";
	var URL="merchant_addDetail.action";
	//获取表单数据
	var formData="merchantId=1234567"
				+"&detail.strType="+$("#sjtype").val()
                +"&detail.strMerchantName="+$("#sjname").val()
                +"&detail.strAdress="+$("#sjaddress").val()
                +"&detail.strPhone="+$("#sjtel").val()
                +"&detail.strTelautogram="+$("#sjfax").val();
                //图片上传遗留问题
               
                //alert(formData);
	ajaxAbstractText(method,URL,formData,parseJson);


}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	$('#sjxxwhbtn').click(function(){addMerchantInfo();});
});
/*================================================henryzhao:商家信息维护  End=================================================================*/
/*================================================henryzhao:商家产品信息维护  start=================================================================*/
function addProduct(){

	//var formId="person_apply_form_id";
	//var infoDlgId="#DlgResult";
	//var infoTextId="#DlgResult_Body";
	var method="GET";
	var URL="merchant_addProduct.action";
	//获取表单数据
	var formData="merchantId=1234567"
				+"&product.strCommodityName="+$("#productName").val()
                +"&product.fprice="+$("#productfPrice").val()
                +"&product.faspotprice="+$("#productfASPOTPrice").val()
                +"&product.strBedType="+$("#productstrBedType").val()
                +"&product.strRoomType="+$("#productstrRoomType").val()
                +"&product.strBreakfast="+$("#productstrBreakfast").val()
                //图片上传遗留问题
                +"&product.strRemarks="+$("#productstrRemarks").val();
               
               // alert(formData);
	ajaxAbstractText(method,URL,formData,parseJson);


}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	//$('#AddOKbtn').click(function(){addProduct();});
});
/*================================================henryzhao:商家产品信息维护  End=================================================================*/
/*================================================henryzhao:产品信息列表  start=================================================================*/
/*function showdialog_zzh(naccountId) {
	var person=findOne(db,"naccountId",naccountId);
	if (person == null)
	 return null;
	//$("naccountId").val("2014");
	//$("#Person_DlgDetail_Remarks").html("nothing");
	$("#myModal1zzhbj_edit_zzh").text(person.strName)
	//if(person.naccountType==0){$("#myModal1zzhbj_edit_zzhlx").text("哈哈")};
	$("#myModal1zzhbj").modal("show");
}
function findOne(db,constr,conval){
	var object;
	$.each(db,
	function(i, item) {
		if (item[constr]==conval) {
			object = item;
			return false;
		}
	});
	return object;
}*/
function hotel_list_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        	var row = $("#template_hotel_list_table_id").clone();//克隆一行出来
			row.find("#template_hotel_list_table_nCommodityId").text(n.ncommodityId);//n.OrderID数据是应哥后台封装过来的数据的名字，n只是代号，OrderID才是需要对应的
			row.find("#template_hotel_list_table_strCommodityName").text(n.strCommodityName); 		
	        row.find("#template_hotel_list_table_fPrice").text(n.fprice);
	        row.find("#template_hotel_list_table_fASPOTPrice").text(n.faspotprice);
	        row.find("#template_hotel_list_table_strRoomType").text(n.strRoomType);
	        row.find("#template_hotel_list_table_strBreakfast").text(n.strBreakfast);
	        row.find("#template_hotel_list_table_operate").html( "<button type='button' class='btn btn-primary' onclick='showdialog_zzh("+n.ncommodityId+")' >编辑</button>&nbsp"+
	                 "<button type='button' class='btn btn-info' onclick='showdialog_zzh("+n.ncommodityId+")'>查看</button>&nbsp" +
	                 		"<button type='button' class='btn btn-danger'>删除</button>");                   
	        row.attr("id","ready");//改变绑定好数据的行的id
	        row.appendTo("#hotel_list_table_id");//添加到模板的容器中
	    });
}
function hotel_list_query()
 {
 	var method="GET";
 	var remote_url="merchant_listProduct.action?merchantId=1234567&pageNo=1&pageSize=10";
	//ajaxAbstract(method_query,URL_query,"",parseQueryJson,dom_handle_query);
	ajaxAbstractJson(method,remote_url,"",hotel_list_parseQueryJson);
}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	
	$('#hotel_list_Btn').click(function(){
		hotel_list_query();});
});
/*================================================henryzhao:产品信息列表  End===================================================*/
/*================================================henryzhao:预制卡状态查询表  start=================================================================*/
function yzkztcx_table_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        	var row = $("#template_yzkztcx_table_id").clone();//克隆一行出来
			row.find("#template_yzkztcx_table_strMembershipCardId").text(n.strMembershipCardId);//n.OrderID数据是应哥后台封装过来的数据的名字，n只是代号，OrderID才是需要对应的
			row.find("#template_yzkztcx_table_bCardCondition").text(n.bcardCondition); 		
	        row.find("#template_yzkztcx_table_strCardMaker").text(n.strCardMaker);
	        row.find("#template_yzkztcx_table_dtCardMakeTime").text(parsedatetime(n.dtCardMakeTime));
	        row.find("#template_yzkztcx_table_strCardSender").text(n.strCardSender);
	        row.find("#template_yzkztcx_table_dtCardSendTime").text(parsedatetime(n.dtCardSendTime));
	        row.find("#template_yzkztcx_table_operate").html( "<button type='button' class='btn btn-primary' onclick='showdialog_yzk("+n.strMembershipCardId+")' >编辑</button>&nbsp"+
	                 "<button type='button' class='btn btn-info' onclick='showdialog_yzk("+n.strMembershipCardId+")'>查看</button>&nbsp" +
	                 		"<button type='button' class='btn btn-danger'>删除</button>");                   
	        row.attr("id","ready");//改变绑定好数据的行的id
	        row.appendTo("#yzkztcx_table_id");//添加到模板的容器中
	    });
}
function showdialog_yzk(naccountId) {
	var person=findOne(db,"naccountId",naccountId);
	if (person == null)
	 return null;
	//$("naccountId").val("2014");
	//$("#Person_DlgDetail_Remarks").html("nothing");
	//$("#myModal1zzhbj_edit_zzh").text(person.strName)
	//if(person.naccountType==0){$("#myModal1zzhbj_edit_zzhlx").text("哈哈")};
	$("#yzk_state_search").modal("show");
}
function findOne(db,constr,conval){
	var object;
	$.each(db,
	function(i, item) {
		if (item[constr]==conval) {
			object = item;
			return false;
		}
	});
	return object;
}
function yzkztcx_table_query()
 {
 	var method="GET";
 	var remote_url;
 	if($("#hykztcx_status_id").val()==1){		//判断发卡状态是已发还是未发
 	remote_url="card_listUnusedCard.action?merchantId=1234567&pageNo=1&pageSize=10";
 	}else if($("#hykztcx_status_id").val()==0) {
 	remote_url="card_listUsedCard.action?merchantId=1234567&pageNo=1&pageSize=10";
 	}
	//ajaxAbstract(method_query,URL_query,"",parseQueryJson,dom_handle_query);
	ajaxAbstractJson(method,remote_url,"",yzkztcx_table_parseQueryJson);
}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	
	$('#yzk_state_btn').click(function(){
		yzkztcx_table_query();});
});
function parsedatetime(datetime){

	var tempdatetime=datetime;
	var resultdatetime=tempdatetime.year+1900+"-"+tempdatetime.month+"-"+tempdatetime.day+" "+tempdatetime.hours+":"+tempdatetime.minutes+":"+tempdatetime.seconds;
	return resultdatetime;
}

/*================================================henryzhao:预制卡状态查询表  End===================================================*/
/*================================================henryzhao:会员卡状态查询表  start=================================================================*/
function hykztcx_table_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        	var row = $("#template_hykztcx_table_id").clone();//克隆一行出来
			row.find("#template_hykztcx_table_strMembershipCardId").text(n.strMembershipCardId);//n.OrderID数据是应哥后台封装过来的数据的名字，n只是代号，OrderID才是需要对应的
			row.find("#template_hykztcx_table_bCardCondition").text(n.bcardCondition); 		
	        row.find("#template_hykztcx_table_strCardMaker").text(n.strCardMaker);
	        row.find("#template_hykztcx_table_dtCardMakeTime").text(parsedatetime(n.dtCardMakeTime));
	        row.find("#template_hykztcx_table_strCardSender").text(n.strCardSender);
	        row.find("#template_hykztcx_table_dtCardSendTime").text(parsedatetime(n.dtCardSendTime));
	        row.find("#template_hykztcx_table_operate").html( "<button type='button' class='btn btn-primary' onclick='showdialog_yzk("+n.strMembershipCardId+")' >编辑</button>&nbsp"+
	                 "<button type='button' class='btn btn-info' onclick='showdialog_yzk("+n.strMembershipCardId+")'>查看</button>&nbsp" +
	                 		"<button type='button' class='btn btn-danger'>删除</button>");                   
	        row.attr("id","ready");//改变绑定好数据的行的id
	        row.appendTo("#hykztcx_table_id");//添加到模板的容器中
	    });
}
function hykztcx_table_query()
 {
 	var method="GET";
 	var remote_url;
 	if($("#hykztcx_status_id").val()==1){		//判断发卡状态是已发还是未发
 	remote_url="card_listUnusedCard.action?merchantId=1234567&pageNo=1&pageSize=10";
 	}else if($("#hykztcx_status_id").val()==0) {
 	remote_url="card_listUsedCard.action?merchantId=1234567&pageNo=1&pageSize=10";
 	}
	//ajaxAbstract(method_query,URL_query,"",parseQueryJson,dom_handle_query);
	ajaxAbstractJson(method,remote_url,"",hykztcx_table_parseQueryJson);
}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	
	$('#hyk_state_btn').click(function(){
		hykztcx_table_query();});
});
/*================================================henryzhao:会员卡状态查询表  End===================================================*/

/*================================================henryzhao:会员卡发放  start=================================================================*/
function vip_card_fafang(){

	//var formId="person_apply_form_id";
	//var infoDlgId="#DlgResult";
	//var infoTextId="#DlgResult_Body";
	var method="GET";
	var URL="card_giveVipCard.action";
	//获取表单数据
	var formData="merchantId=1234567"
				+"&vipInfo.strMenbershipName="+$("#vipnamefafang").val()
                +"&vipInfo.strPapersId="+$("#idnumfafang").val();
               // alert(formData);
	ajaxAbstractText(method,URL,formData,parseJson);


}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	$('#vip_fafang').click(function(){vip_card_fafang();});
});
/*================================================henryzhao:会员卡发放  End=================================================================*/
/*================================================henryzhao:会员卡补发  start=================================================================*/
function vip_card_bufa(){

	//var formId="person_apply_form_id";
	//var infoDlgId="#DlgResult";
	//var infoTextId="#DlgResult_Body";
	var method="GET";
	var URL="card_giveVipCard.action";
	//获取表单数据
	var formData="merchantId=1234567"
				+"&vipInfo.strMenbershipName="+$("#vipnamefafang").val()
                +"&vipInfo.strPapersId="+$("#idnumfafang").val();
               // alert(formData);
	ajaxAbstractText(method,URL,formData,parseJson);


}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	$('#vip_fafang').click(function(){vip_card_fafang();});
});
/*================================================henryzhao:会员卡补发  End=================================================================*/
/*================================================henryzhao:会员信息添加  start=================================================================*/
function vip_info_add(){

	//var formId="person_apply_form_id";
	//var infoDlgId="#DlgResult";
	//var infoTextId="#DlgResult_Body";
	var method="GET";
	var URL="card_changeVIPInfo.action";

	//证件类型
	var tempType=$("#card_info_type").val();
	var operate="信息修改";
				var reason="信息修改";
				var merchantName="1";
	//获取表单数据
	var formData=operate+reason+merchantName
				+"&vipInfo.strMenbershipName="+$("#vipinfoname").val()//会员姓名
				+"&vipInfo.nMembershipId="+$("#vipinfocardnum").val()//会员卡号
				+"&vipInfo.strPapersTypeChange="+tempType//证件类型
				+"&vipInfo.strPapersId="+$("#infocardnum").val()//证件号码
				+"&vipInfo.strSex="+$("#vip_info_gender").val()//性别
				+"&vipInfo.strCellphone="+$("#vipinfophonenum").val()//会员手机号
				+"&vipInfo.strEmail="+$("#vipinfomail").val()//电子邮箱
				+"&vipInfo.strMenbershipAdress="+$("#vipinfoaddress").val();//会员地址
				

//                alert(formData);
	ajaxAbstractText(method,URL,formData,parseJson);


}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	$('#vip_info_submit_btn').click(function(){vip_info_add();});
});
/*================================================henryzhao:会员信息添加  End=================================================================*/
/*================================================henryzhao:会员手机号码修改  start=================================================================*/
function vip_info_change_phonenum(){

	//var formId="person_apply_form_id";
	//var infoDlgId="#DlgResult";
	//var infoTextId="#DlgResult_Body";
	var method="GET";
	var URL="card_changeVIPInfo.action";

	
	//获取表单数据
	var formData="merchantId=1234567"
				+"&vipInfo.strCellphone="+$("#vipinfophonenum_new").val();//会员手机号
			
                //alert(formData);
	ajaxAbstractText(method,URL,formData,parseJson);


}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	$('#vip_info_change_phonenum_save').click(function(){vip_info_change_phonenum();});
});
/*================================================henryzhao:会员手机号码修改  End=================================================================*/
/*================================================henryzhao:会员变更历史表  End=================================================================*/

/*<table class="table" id="hyxxbglsb_table_id">
					<thead>
					  <tr>
						<th width="10%">会员号</th>
						<th width="20%">地址</th>
						<th width="20%">手机号码</th>
						<th width="20%">电子邮箱</th>
						<th width="20%">操作时间</th>
						<th width="10%">操作人</th>
					  </tr>
					</thead>
					<tbody>
					  <tr id="template_hyxxbglsb_table_id">
						<td id="hyxxbglsb_table_nRecordId">1</td>
						<td id="hyxxbglsb_table_strMenbershipAdress">地址</td>
						<td id="hyxxbglsb_table_strCellphone">13247107171</td>
						<td id="hyxxbglsb_table_strEmail">13268807788</td>
						<td id="hyxxbglsb_table_dtOperateTime">2014-07-14</td>
						<td id="hyxxbglsb_table_strOperator">谢mm</td>
					  </tr>
					  <tr>
						<td>2</td>
						<td>手机号码</td>
						<td>14996859639</td>
						<td>13247107171</td>
						<td>2014-07-11</td>
						<td>谢mm</td>
					  </tr>
					</tbody>
				  </table>
*/
function hyxxbglsb_table_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        	var row = $("#template_hyxxbglsb_table_id").clone();//克隆一行出来
			row.find("#hyxxbglsb_table_nRecordId").text(n.nrecordId);//n.OrderID数据是应哥后台封装过来的数据的名字，n只是代号，OrderID才是需要对应的
			row.find("#hyxxbglsb_table_strMenbershipAdress").text(n.strMenbershipAdress); 		
	        row.find("#hyxxbglsb_table_strCellphone").text(n.strCellphone);
	        row.find("#hyxxbglsb_table_strEmail").text(n.strEmail);
	        row.find("#hyxxbglsb_table_dtOperateTime").text(parsedatetime(n.dtOperateTime));
	        row.find("#hyxxbglsb_table_strOperator").text(n.strOperator);                 
	        row.attr("id","ready");//改变绑定好数据的行的id
	        row.appendTo("#hyxxbglsb_table_id");//添加到模板的容器中
	    });

}

function hyxxbglsb_table_query()
 {
 	var method="GET";
 	var remote_url;
 	
 	remote_url="card_listInfoChange.action?vipId=10001&pageNo=1&pageSize=10";
 	
	//ajaxAbstract(method_query,URL_query,"",parseQueryJson,dom_handle_query);
	ajaxAbstractJson(method,remote_url,"",hyxxbglsb_table_parseQueryJson);
}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	
	$('#vip_details_change_Btn').click(function(){
		hyxxbglsb_table_query();
		});
	$('#vip_details_change_Btn1').click(function(){
		hyxxbglsb_table_query();});
	$('#hyxxbglsb_table_id tr').hover(function(){
	    $(this).children('td').addClass('hover')
	    }, function(){
	        $(this).children('td').removeClass('hover')
	        });
	$('#hyxxbglsb_table_id tbody tr:odd').css('background-color', '#42426f');
	//$('#hyxxbglsb_table_id tbody tr:even').css('background-color','#0000ff');
	$("#hyxxbglsb_table_id tbody tr:odd").addClass("odd");
	$("#hyxxbglsb_table_id tbody tr:even").addClass("even");

});
/*================================================henryzhao:会员变更历史表  End========================================================*/
/*================================================henryzhao:会员手机号码修改  start=================================================================*/
function vip_info_change_phonenum(){

	//var formId="person_apply_form_id";
	//var infoDlgId="#DlgResult";
	//var infoTextId="#DlgResult_Body";
	var method="GET";
	var URL="card_changeVIPInfo.action";

	
	//获取表单数据
	var formData="merchantId=1234567"
				+"&vipInfo.strCellphone="+$("#vipinfophonenum_new").val();//会员手机号
			
                //alert(formData);
	ajaxAbstractText(method,URL,formData,parseJson);


}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	$('#vip_info_change_phonenum_save').click(function(){vip_info_change_phonenum();});
});
/*================================================henryzhao:会员手机号码修改  End=================================================================*/
/*================================================henryzhao:会员充值  start=================================================================*/
function vip_recharge(){

	//var formId="person_apply_form_id";
	//var infoDlgId="#DlgResult";
	//var infoTextId="#DlgResult_Body";
	var method="GET";
	var URL="card_cardRecharge.action";

	
	//获取表单数据
	var formData="merchantId=1234567"
				+"&vipId="+$("#rechargevipid").val()
				+"&balance="+$("#chargemoney").val();
			
               // alert(formData);
	ajaxAbstractText(method,URL,formData,parseJson);
	$("#rechargevipid").empty();
	$("#chargemoney").empty();


}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	$('#rechargevipid_btn').click(function(){vip_recharge();});
});
/*================================================henryzhao:会员充值  End=================================================================*/
/*================================================henryzhao:会员充值历史查询表  End=================================================================*/

/*<table class="table" id="hyczlscxb_table_id">
					<thead>
					  <tr>
						<th width="20%">充值编号</th>
            			<th width="20%">会员卡卡号</th>
            			<th width="20%">充值金额</th>
            			<th width="20%">实际可用金额</th>
            			<th width="10%">操作时间</th>
            			<th width="10%">操作人</th>
					  </tr>
					</thead>
					<tbody>
					  <tr id="template_hyczlscxb_table_id">
						<td id="hyczlscxb_table_nRecordId"></td>
						<td id="hyczlscxb_table_nMembershiId"></td>
						<td id="hyczlscxb_table_strRechargeSum"></td>
						<td id="hyczlscxb_table_strRechargeSumNow"></td>
						<td id="hyczlscxb_table_dtOperateTime"></td>
						<td id="hyczlscxb_table_strOperator"></td>
					  </tr>
					  
					</tbody>
				  </table>
*/
function hyczlscxb_table_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        	var row = $("#template_hyczlscxb_table_id").clone();//克隆一行出来
			row.find("#hyczlscxb_table_nRecordId").text(n.nrecordId);//n.OrderID数据是应哥后台封装过来的数据的名字，n只是代号，OrderID才是需要对应的
		 		
	        row.find("#hyczlscxb_table_strRechargeSum").text(n.strRechargeSum);
	        row.find("#hyczlscxb_table_strRechargeSumNow").text(n.strRechargeSumNow);
	        row.find("#hyxxbglsb_table_dtOperateTime").text(parsedatetime(n.dtOperateTime));
	        row.find("#hyxxbglsb_table_strOperator").text(n.strOperator);                 
	        row.removeAttr("id");//改变绑定好数据的行的id
	        row.appendTo("#hyczlscxb_table_id");//添加到模板的容器中
	    });
}
function hyczlscxb_table_query()
 {
 	var method="GET";
 	var remote_url;
 	
 	remote_url="card_listRechargeHistory.action?vipId=10001&pageNo=1&pageSize=10";
 	
	//ajaxAbstract(method_query,URL_query,"",parseQueryJson,dom_handle_query);
	ajaxAbstractJson(method,remote_url,"",hyczlscxb_table_parseQueryJson);
}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	
	$("#vip_charge_history_btn").click(function(){
		hyczlscxb_table_query();});
});
/*================================================henryzhao:会员充值历史查询  End========================================================*/
/*================================================henryzhao:会员date充值历史查询表  End=================================================================*/

/*<table class="table" id="hyczlscxb_table_id">
					<thead>
					  <tr>
						<th width="20%">充值编号</th>
            			<th width="20%">会员卡卡号</th>
            			<th width="20%">充值金额</th>
            			<th width="20%">实际可用金额</th>
            			<th width="10%">操作时间</th>
            			<th width="10%">操作人</th>
					  </tr>
					</thead>
					<tbody>
					  <tr id="template_hyczlscxb_table_id">
						<td id="hyczlscxb_table_nRecordId"></td>
						<td id="hyczlscxb_table_nMembershiId"></td>
						<td id="hyczlscxb_table_strRechargeSum"></td>
						<td id="hyczlscxb_table_strRechargeSumNow"></td>
						<td id="hyczlscxb_table_dtOperateTime"></td>
						<td id="hyczlscxb_table_strOperator"></td>
					  </tr>
					  
					</tbody>
				  </table>
*/
function date_hyczlscxb_table_parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
        	var row = $("#date_template_hyczlscxb_table_id").clone();//克隆一行出来
			row.find("#date_hyczlscxb_table_nRecordId").text(n.nrecordId);//n.OrderID数据是应哥后台封装过来的数据的名字，n只是代号，OrderID才是需要对应的
			row.find("#date_hyczlscxb_table_nMembershiId").text(n.nMembershiId); 		
	        row.find("#date_hyczlscxb_table_strRechargeSum").text(n.strRechargeSum);
	        row.find("#date_hyczlscxb_table_strRechargeSumNow").text(n.strRechargeSumNow);
	        row.find("#date_hyxxbglsb_table_dtOperateTime").text(parsedatetime(n.dtOperateTime));
	        row.find("#date_hyxxbglsb_table_strOperator").text(n.strOperator);                 
	        row.removeAttr("id");//改变绑定好数据的行的id
	        row.appendTo("#date_hyczlscxb_table_id");//添加到模板的容器中
	    });
}
function date_hyczlscxb_table_query()
 {
 	var method="GET";
 	var remote_url;
 	
 	remote_url="card_listRechargeHistory.action?vipId=10001&pageNo=1&pageSize=10";
 	
	//ajaxAbstract(method_query,URL_query,"",parseQueryJson,dom_handle_query);
	ajaxAbstractJson(method,remote_url,"",date_hyczlscxb_table_parseQueryJson);
}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	
	$("#date_charge_details_btn").click(function(){
		date_hyczlscxb_table_query();});
});
/*================================================henryzhao:会员date充值历史查询  End========================================================*/
/*================================================henryzhao:现金消费  start=================================================================*/
function cash_shopping(){
	var cash_num=$("#shoppingmoney").val();
	alert("恭喜你，已经成功消费"+cash_num+"元！");
	$("#shoppingmoney").val("");
}
/*================================================henryzhao:现金消费  End=================================================================*/
/*================================================henryzhao:联盟消费  start=================================================================*/
function rainbow_shopping(){
	var cash_num=$("#shoppingmoney1").val();
	alert("恭喜你，已经成功消费"+cash_num+"元！");
	$("#shoppingmoney1").val("");
	$("#current_num").empty();
	$("#current_num").html("<lable>"+(2655-cash_num)+"元</lable>");

}
/*================================================henryzhao:联盟消费  End=================================================================*/








/*================================================henryzhao:图片上传  Start========================================================*/

$("#sjtpllbtn").change(function(){
        var objUrl = getObjectURL(this.files[0]) ;
         console.log("objUrl = "+objUrl) ;
          if (objUrl) {
           $("#showImage").attr("src", objUrl) ;
  }
}) ;
$("#productllbtn").change(function(){
        var objUrl = getObjectURL(this.files[0]) ;
         console.log("objUrl = "+objUrl) ;
          if (objUrl) {
           $("#showImagePro").attr("src", objUrl) ;
  }
}) ;
function getObjectURL(file) {
  var url = null ; 
  if (window.createObjectURL!=undefined) { // basic
    url = window.createObjectURL(file) ;
  } else if (window.URL!=undefined) { // mozilla(firefox)
    url = window.URL.createObjectURL(file) ;
  } else if (window.webkitURL!=undefined) { // webkit or chrome
    url = window.webkitURL.createObjectURL(file) ;
  }
  return url ;
}
    function uploadImage() { 
    
      $(document).ready(function() {
        var options = { 
        url : "merchant_updateMerchantDetail.action?detail.nrecordId=8",//跳转到相应的Action 
        type : "get",//提交方式 
        dataType : "script",//数据类型 
        success : function(msg) {//调用Action后返回过来的数据 
                 alert(msg); 
                 if (msg.indexOf("#") > 0) { 
                  } else { 
                    $("#message").html(msg);//在相应的位置显示信息 
                    } 
                } 
          }; 
        $("#sjglpzform").ajaxSubmit(options);//绑定页面中form表单的id 
                       return false; 
      }); 
  } 
  function uploadImagePro() { 
    
      $(document).ready(function() {
        var options = { 
        url : "merchant_addProduct.action?merchantId=1234567",//跳转到相应的Action 
        type : "get",//提交方式 
        dataType : "script",//数据类型 
        success : function(msg) {//调用Action后返回过来的数据 
                 alert(msg); 
                 if (msg.indexOf("#") > 0) { 
                  } else { 
                    $("#message").html(msg);//在相应的位置显示信息 
                    } 
                } 
          }; 
        $("#product_hotel_form_id").ajaxSubmit(options);//绑定页面中form表单的id 
                       return false; 
      }); 
  }
/*================================================henryzhao:图片上传  End========================================================*/ 