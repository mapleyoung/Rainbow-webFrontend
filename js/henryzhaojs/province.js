﻿$(document).ready(function () {
    //加载省份列表信息
    $.ajax({
        url: "province_city.xml",
        dataType: "xml",
        success: function (xml) {
            $(xml).find("province").each(function () {                                                  //找到(province)省份节点;
                $("<option></option>").html($(this).attr("name")).appendTo("#province");             //加载(province)省份信息到列表中
                $("<option></option>").html($(this).attr("name")).appendTo("#province1");             //加载(province)省份信息到列表中
            })
        }
    })
    //省份列表信息更改时，加载城市列表信息
    $("#province").change(function () {
        var value = $("#province").val();                                                            //省份值;
        if (value != "请选择") {
            $("#city").css("display", "none").find("option").remove();                              //显示城市下拉列表框删除城市下拉列表中的数据;
            $("#city").html("<option>请选择</option>");                                              //加载城市列表中的请选择;
            $("#district").find("option").remove();                                                      //删除地区下拉列表中的数据;
            $("#district").html("<option>请选择</option>")                                               //加载地区列表中的请选择;
            $.ajax({
                url: "province_city.xml",
                dataType: "xml",
                success: function (xml) {
                    $(xml).find("[name='" + value + "']").find("city").each(function () {               //根据省份name属性得到子节点City节点name属性;
                        $("<option></option>").html($(this).attr("name")).appendTo("#city");         //加载City(城市)信息到下拉列表中;
                    })
                }
            })
        }
    })
//省份列表信息更改时，加载城市列表信息
    $("#province1").change(function () {
        var value = $("#province1").val();                                                            //省份值;
        if (value != "请选择") {
            $("#city1").css("display", "none").find("option").remove();                              //显示城市下拉列表框删除城市下拉列表中的数据;
            $("#city1").html("<option>请选择</option>");                                              //加载城市列表中的请选择;
            $("#district1").find("option").remove();                                                      //删除地区下拉列表中的数据;
            $("#district1").html("<option>请选择</option>")                                               //加载地区列表中的请选择;
            $.ajax({
                url: "province_city.xml",
                dataType: "xml",
                success: function (xml) {
                    $(xml).find("[name='" + value + "']").find("city").each(function () {               //根据省份name属性得到子节点City节点name属性;
                        $("<option></option>").html($(this).attr("name")).appendTo("#city1");         //加载City(城市)信息到下拉列表中;
                    })
                }
            })
        }
    })
    //城市列表信息改变时，加载地区列表信息
    $("#city").change(function () {
        var value = $("#city").val();                                                                //城市值;
        if (value != "请选择") {
            $("#district").css("display", "none").find("option").remove();                              //显示地区下拉列表框删除地区下拉列表中的数据;
            $("#district").html("<option>请选择</option>");                                              //加载地区列表中的请选择;
            $.ajax({
                url: "province_city.xml",
                dataType: "xml",
                success: function (xml) {
                    $(xml).find("[name='" + value + "']").find("area").each(function () {            //根据城市节点name得到子节点Area节点name属性;
                        $("<option></option>").html($(this).attr("name")).appendTo("#district");         //加载到Area(地区)下拉列表中;
                    })
                }
            })
        }
    })
    //城市列表信息改变时，加载地区列表信息
    $("#city1").change(function () {
        var value = $("#city1").val();                                                                //城市值;
        if (value != "请选择") {
            $("#district1").css("display", "none").find("option").remove();                              //显示地区下拉列表框删除地区下拉列表中的数据;
            $("#district1").html("<option>请选择</option>");                                              //加载地区列表中的请选择;
            $.ajax({
                url: "province_city.xml",
                dataType: "xml",
                success: function (xml) {
                    $(xml).find("[name='" + value + "']").find("area").each(function () {            //根据城市节点name得到子节点Area节点name属性;
                        $("<option></option>").html($(this).attr("name")).appendTo("#district1");         //加载到Area(地区)下拉列表中;
                    })
                }
            })
        }
    })
});