/**
*公共函数，空值，网络连接等
*
*/
function NotifyEmptyResult() {
	alert("结果为空！");
}
function NotifyTryAgain() {
	//alert("网络出错，请稍后再试！");
}
function initData(){
	
}
/**
*公共请求函数
*/
function ajaxAbstract(method,remote_url,send_data,parser_callback,dom_callback){
	$.ajax({
		type: method,
		url: remote_url,
		dataType: 'json',
		data:send_data,
		success: function(result) {
			if (result == "" || result == null) {
				NotifyEmptyResult();
			} else {
				dom_callback(parser_callback(result));
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			NotifyTryAgain();
		}
	});
}



	//商家入盟
function grayMidWhite(){
	var method="post";
	var URL="merchant_Register.action";
	

	//获取表单数据
	var formData="regInfo.strMerchantType="+$("#merchant_type").text()
                +"&regInfo.strMerchantName="+$("#enterpriseName").val()
                +"&regInfo.strAdress="+$("#concreteAddress").val()
                +"&regInfo.strMerchantPhone="+$("#enterpriseTel").val()
                +"&regInfo.strZipcode="+$("#zipcode").val()
                +"&regInfo.nStar="+$("#merchant_star").text()
                +"&regInfo.nComsunPtionpratio="+$("#percentage").val()
                +"&regInfo.strTitle="+$("#sex").text()
                +"&regInfo.strName="+$("#contactName").val()
                +"&regInfo.strPosition="+$("#contactJob").val()
                +"&regInfo.strPhone="+$("#contactTel").val()
                +"&regInfo.strCellphone="+$("#cellphone").val()
                +"&regInfo.strTelautogram="+$("#telautogram").val()
                +"&regInfo.strEmail="+$("#email").val();

                alert(formData);
	ajaxAbstract(method,URL,formData,parseJson,DomInsert);


}

//dom插入
function DomInsert(str)
{
	var $boxes = $(str);
	//var $newInsert = $('#sample').html( $boxes );
	showDlg(infoDlgId,infoTextId,str);
}
//格式化数据
function parseJson(data)
{
	//var html="";
	//$.each(data.res,function(i,item){
	//	html=html+"<tr><td>"+item.name+"</td><td>"+item.percetage+"</td></tr>"
	//});
	return data.state;
}

/*
henryzhao：改为在bobo.js 617行进行函数调用

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	$('#amButton').click(function(){grayMidWhite();});

});
*/

