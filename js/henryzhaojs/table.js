/*================================================henryzhao:子账号列表  start hah===================================================*/

var db;
var item;

function NotifyEmptyResult() {
	alert("结果为空！");
}

function initData(){
	
}
function showDlg(Dlgid,textId,msg){
	$(textId).html(msg);
	$(Dlgid).modal("show");
}
function ajaxAbstractJson(method,remote_url,send_data,parser_callback){
	  
    $.ajax({
        type: "get",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
        url: remote_url,//要访问的后台地址
        data: send_data,
        complete :function(){},//AJAX请求完成时隐藏loading提示
        success: function(msg){        //msg为返回的数据，在这里做数据绑定
        	db=msg;
        	if (msg == "" || msg == null) {
				NotifyEmptyResult();
			} else {
				parser_callback(msg);
	           
	            //$("[@id=child_Account_table_template]").show();
	            		
    		}
    },
        error: function(xhr, textStatus, errorThrown) {
			//NotifyTryAgain();
		}
    });

}

/*================================================henryzhao:子账号列表  start===================================================*/
function showdialog(naccountId) {
	var person=findOne(db,"naccountId",naccountId);
	if (person == null)
	 return null;
	//$("naccountId").val("2014");
	//$("#Person_DlgDetail_Remarks").html("nothing");
	$("#myModal1zzhbj_edit_zzh").text(person.strName);
	if(person.binUse==true)
		$("#myModal1zzhbj_edit_binuse").attr("checked");
	//if(person.naccountType==0){$("#myModal1zzhbj_edit_zzhlx").text("哈哈")};
	$("#myModal1zzhbj").modal("show");
}
function findOne(db,constr,conval){
	var object;
	$.each(db,
	function(i, item) {
		if (item[constr]==conval) {
			object = item;
			return false;
		}
	});
	return object;
}
function parseQueryJson(msg)
{
	 $.each(msg, function(i, n){
     	var tempType;
        	var row = $("#child_Account_table_template").clone();//克隆一行出来
			row.find("#child_Account_table_nAccountId").text(n.naccountId);//n.OrderID数据是应哥后台封装过来的数据的名字，n只是代号，OrderID才是需要对应的
			if(n.naccountType==0){			//对账户类型进行判断
 			tempType="前台";
 			}else if(n.naccountType==1){
 				tempType="管理员";
 			}else if(n.naccountType==2){
 				tempType="客户经理";
 			}
 		row.find("#child_Account_table_naccountType").text(tempType); 		
        row.find("#child_Account_table_strName").text(n.strName);
        row.find("#child_Account_table_strPasswd").text(n.strPasswd);
        row.find("#child_Account_table_bInUse").html( "<input name='checkbox' type='checkbox' disabled data-toggle='switch'/>");
        row.find("#child_Account_table_operate").html( "<button type='button' class='btn btn-primary' onclick='showdialog("+n.naccountId+")' >编辑</button>&nbsp"+
                 "<button type='button' class='btn btn-info' onclick='showdialog("+n.naccountId+")'>查看</button>&nbsp" +
                 		"<button type='button' class='btn btn-danger'>删除</button>");                   
        row.attr("id","ready");//改变绑定好数据的行的id
        row.appendTo("#datas");//添加到模板的容器中
    });
}
function childAccount_query()
 {
 	var method="GET";
 	var remote_url="merchant_listChild.action?merchantId=1234567&pageNo=1&pageSize=10";
	//ajaxAbstract(method_query,URL_query,"",parseQueryJson,dom_handle_query);
	ajaxAbstractJson(method,remote_url,"",parseQueryJson);
}

//添加button绑定事件
$(document).ready(function() {
	//person_apply();
	
	$('#AddSubcount_test_Btn').click(function(){
		childAccount_query();});
});
/*================================================henryzhao:子账号列表  End===================================================*/






