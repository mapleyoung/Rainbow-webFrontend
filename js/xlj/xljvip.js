      
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~个人账户~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$(function(){

        $("#strPapersId").blur(function(){

             var strPapersId=document.getElementById("strPapersId").value;
             var strPapersIdcheck1=strPapersId=="";
             var strPapersIdcheck2= /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(strPapersId);
             if(strPapersIdcheck1){
                 $("#hidestrPapersId").html("<label>*请输入会员身份证号</label>")
             }
             else if(!strPapersIdcheck2)
                 {  
                    $("#hidestrPapersId").html("<label>*请输入有效的身份证号</label>");
                    $("#hidestrPapersId").show();
                }
            else if(strPapersIdcheck2)
                $("#hidestrPapersId").children().empty();   
        })
})
//手机号
$(function(){
            $("#strCellphone").blur(function(){
            
             var strCellphone=document.getElementById("strCellphone").value;
             var strCellphonecheck1=strCellphone=="";
             var strCellphonecheck2=/^1[3|5|8]\d{9}$/.test(strCellphone);
             if(strCellphonecheck1){
                 $("#hidestrCellphone").html("<label>*请输入会员手机号</label>")
             }
             else if(!strCellphonecheck2)
                 {  
                    $("#hidestrCellphone").html("<label>*请输入有效的手机号</label>");
                    $("#hidestrCellphone").show();
                }
            else if(strCellphonecheck2)
                $("#hidestrCellphone").children().empty();   
        })
});
//会员电子邮箱
$(function(){
   
            $("#strEmail").blur(function(){
            
             var strEmail=document.getElementById("strEmail").value;
             var strEmailcheck1=strEmail=="";
             var strEmailcheck2= /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(strEmail);
             if(strEmailcheck1){
                 $("#hidestrEmail").html("<label>*请输入会员电子邮箱</label>")
             }
             else if(!strEmailcheck2)
                 {  
                    $("#hidestrEmail").html("<label>*请输入有效的邮箱</label>");
                    $("#hidestrEmail").show();
                }
            else if(strEmailcheck2)
                $("#hidestrEmail").children().empty();   
        })
});
//登陆密码（）设定为>=6的数字串
$(function(){
            $("#strPasswd").blur(function(){
            strPasswd = document.getElementById("strPasswd").value;
                if(strPasswd==""){
                $("#hidestrPasswd").html("<label>*请输入子账号密码</label>")
                }
                else{
                    if(isNaN(strPasswd)||strPasswd.length<6) 
                        {  
                        $("#hidestrPasswd").html("<label>*请输入不少于6位的数字</label>")
                        }
                    else {
                        $("#hidestrPasswd").children().empty();
                        }
                }
            })
});
//交易密码（）设定为=6的数字串

$(function(){
            $("#strTradingPasswd").blur(function(){
            strTradingPasswd = document.getElementById("strTradingPasswd").value;
                if(strTradingPasswd==""){
                $("#hidestrTradingPasswd").html("<label>*请输入子账号密码</label>")
                }
                else{
                    if(isNaN(strTradingPasswd)||strTradingPasswd.length!=6) 
                        {  
                        $("#hidestrTradingPasswd").html("<label>*请输入长度为6位的数字</label>")
                        }
                    else {
                        $("#hidestrTradingPasswd").children().empty();
                        }
                }
            })
});



//提现积分
$(function(){
            $("#strBankAccount_nCostPoints").blur(function(){
            strBankAccount_nCostPoints = document.getElementById("strBankAccount_nCostPoints").value;
                if(strBankAccount_nCostPoints==""){
                        $("#hidestrBankAccount_nCostPoints").html("<label>*请输入您的提现积分</label>");
                        $("#hidestrBankAccount_nCostPoints").show();
                }
                
                 if(strBankAccount_nCostPoints!=""){
                         $("#hidestrBankAccount_nCostPoints").hide();
                }
                
            })
});
//银行卡号
$(function(){
            $("#strBankAccount_strBankAccount").blur(function(){
            strBankAccount_strBankAccount = document.getElementById("strBankAccount_strBankAccount").value;
                if(strBankAccount_strBankAccount==""){
                        $("#hidestrBankAccount_strBankAccount").html("<label>*请输入您的银行卡号</label>");
                        $("#hidestrBankAccount_strBankAccount").show();
                }
                
                 if(strBankAccount_strBankAccount!=""){
                         $("#hidestrBankAccount_strBankAccount").hide();
                }
                
            })
});
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~邮寄方式~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$("#PointsToPresent_strWayOfReceive").change(function(){
    
    var text = $("#PointsToPresent_strWayOfReceive").val();
    if(text=="上门领取"){
        $("#youji_dizhi").hide();
    }
    else if(text=="邮寄"){
        $("#youji_dizhi").show();
    }
});
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~提现方式~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$("#PointsToCash_strWayOfReceive").change(function(){
    
    var text = $("#PointsToCash_strWayOfReceive").val();
    if(text=="联盟储值"){
        $("#yinghang_kahao").hide();
    }
    else if(text=="银行转账"){
        $("#yinghang_kahao").show();
    }
});
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~提现积分~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$(function(){
            $("#PointsToCash_nCostPoints").blur(function(){
                
            PointsToCash_nCostPoints = $("#PointsToCash_nCostPoints").val();
                if(PointsToCash_nCostPoints==""){
                $("#hidePointsToCash_nCostPoints").html("<label>*请输入你要提现的积分</label>")
                }
                else{
                  
                    var totalpoints=parseInt($("#totalpoints").text());
                    if(PointsToCash_nCostPoints>totalpoints) 
                        {  
                        $("#hidePointsToCash_nCostPoints").html("<label>*提现积分不能超过总积分</label>")
                        }
                    else {
                        $("#hidePointsToCash_nCostPoints").children().empty();
                        }
                }
            })
});
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~提现金额~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$("#PointsToCash_nCostPoints").change(function(){
    PointsToCash_nCostPoints = $("#PointsToCash_nCostPoints").val();
    var totalpoints=parseInt($("#totalpoints").text());
    if(PointsToCash_nCostPoints<=totalpoints) 
                        {  
                        $("#tixian_money").text(PointsToCash_nCostPoints/2);
                        }
    
});
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~要换这个~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

$(".mybtn").click(function(){
if($(this).text()=="要换这个"){
        $(this).attr("class","btn-warning btn btn-primary btn-large btn-block ")
        $(this).text("兑换成功")
   }
else if($(this).text()=="兑换成功"){
        $(this).attr("class","btn btn-primary btn-large btn-block mybtn")
        $(this).text("要换这个")
   }
 
   
});

 

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~滑轮~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  var scrollFunc = function (e) {
  
        var direct = 0;
        e = e || window.event;
       if (e.detail) {  //Firefox滑轮事件
       
            if (e.detail> 0) { //当滑轮向上滚动时
                $("#daohang").fadeOut(300);
                s = $(document).scrollTop();
    a = $("#personal_count").offset().top;
    b = $("#personal_count_history").offset().top-600;
    c = $("#gifts").offset().top-600;
    d = $("#money").offset().top-600;
    e = $("#custom_history").offset().top-600;
   
    if(s<b){
        //账号管理
          $('ul li').removeClass("todo-done");
          $("#daohang1").toggleClass("todo-done");
                
    }
    else if(s>=b&&s<c){
          $('ul li').removeClass("todo-done");
          $("#daohang2").toggleClass("todo-done");
    }
    else if(s<d){
          $('ul li').removeClass("todo-done");
          $("#daohang3").toggleClass("todo-done");
    }
    else if(s<e){
           $('ul li').removeClass("todo-done");
          $("#daohang4").toggleClass("todo-done");
    }
  
    else{
           $('ul li').removeClass("todo-done");
          $("#daohang5").toggleClass("todo-done");
    }
    
            }
             if (e.detail< 0) { //当滑轮向下滚动时
                $("#daohang").fadeIn(300);
                
                s = $(document).scrollTop();
    a = $("#personal_count").offset().top;
    b = $("#personal_count_history").offset().top-200;
    c = $("#gifts").offset().top-200;
    d = $("#money").offset().top-200;
    e = $("#custom_history").offset().top-200;
    if(s<b){
           $('ul li').removeClass("todo-done");
          $("#daohang1").toggleClass("todo-done");
                
    }
    else if(s>=b&&s<c){
           $('ul li').removeClass("todo-done");
          $("#daohang2").toggleClass("todo-done");
        
    }
    else if(s<d){
         $('ul li').removeClass("todo-done");
          $("#daohang3").toggleClass("todo-done");
    }
    else if(s<e){
         $('ul li').removeClass("todo-done");
          $("#daohang4").toggleClass("todo-done");
    }
    
    else{
           $('ul li').removeClass("todo-done");
          $("#daohang5").toggleClass("todo-done");
    }
             }
        }
        ScrollText(direct);
    }
    //给页面绑定滑轮滚动事件
    if (document.addEventListener) {
        document.addEventListener('DOMMouseScroll', scrollFunc, false);
    }
    //滚动滑轮触发scrollFunc方法
    window.onmousewheel = document.onmousewheel = scrollFunc;  
        