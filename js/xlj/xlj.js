      
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~子账号管理~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//确认添加子账号按钮
$(function(){
		     $("#qrAddSubcountBtn").click(function () {
		          	
			var zzh = document.getElementById("zzh").value;
			var zzhmm = document.getElementById("zzhmm").value;
			var zzhcheck1=zzh=="";
			var zzhcheck2=/^13\d{9}$/g.test(zzh)||(/^15[8,9]\d{8}$/g.test(zzh));
			var zzhmmcheck1=zzhmm=="";
			var zzhmmcheck2=isNaN(zzhmm)||zzhmm.length<6;
                //验证子账号
			    if(zzhcheck1){
				      $("#hidezzh").html("<label>*请输入子账号</label>")
				}
				else{
					if(zzhcheck2) 
					    {  
						$("#hidezzh2").hide();
						$("#hidezzh1").hide();
						
						}
					else {
						$("#hidezzh2").show();
						$("#hidezzh1").hide();
						}
				}
		         ;
		          //验证子账号密码
			    if(zzhmmcheck1){
				$("#hidezzhmm1").show();
				$("#hidezzhmm2").hide();
				}
				else{
					if(zzhmmcheck2) 
					    {  
						$("#hidezzhmm2").show();
						$("#hidezzhmm1").hide();
						}
					else {
						$("#hidezzhmm2").hide();
						$("#hidezzhmm1").hide();
						
						}
				}
               if(zzhcheck2&&!zzhmmcheck1&&!zzhmmcheck2)
               	   location.hash="#bussiness_count";
		      })
});
//子账号（手机号）
$(function(){
			$("#zzh").blur(function(){
			zzh = document.getElementById("zzh").value;
			    if(zzh==""){
                      $("#hidezzh").html("<label>*请输入子账号</label>")
				}
				else{
					if(/^13\d{9}$/g.test(zzh)||(/^15[8,9]\d{8}$/g.test(zzh))) 
					    {  
						$("#hidezzh").children().empty();
						}
					else {
						$("#hidezzh").html("<label>*请输入正确格式的子账号（手机号）</label>")
						}
				}
			})
});
//子账号密码（）设定为>=6的数字串
$(function(){
			$("#zzhmm").blur(function(){
			zzhmm = document.getElementById("zzhmm").value;
			    if(zzhmm==""){
				$("#hidezzhmm").html("<label>*请输入子账号密码</label>")
				}
				else{
					if(isNaN(zzhmm)||zzhmm.length<6) 
					    {  
						$("#hidezzhmm").html("<label>*请输入不少于6位的数字</label>")
						}
					else {
						$("#hidezzhmm").children().empty();
						}
				}
			})
});
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~商家信息维护~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//商家名称
$(function(){
        $("#sjname").blur(function(){
             var sjname=document.getElementById("sjname").value;
             var sjnamecheck1=sjname=="";
             var sjnamecheck2=/^[a-zA-Z\u4e00-\u9fa5\s\d]{1,20}$/;
             if(sjnamecheck1){
             	 $("#hidesjname").html("<label>*请输入商家名称</label>")
             }
             else if(!sjnamecheck2.test(sjname))
				 {  
					$("#hidesjname").html("<label>*请输入正确格式的商家名称</label>")
				}
            else{
                $("#hidesjname").children().empty();
            }
        })
})
//商家地址
$(function(){
        $("#sjaddress").blur(function(){
             var sjaddress=document.getElementById("sjaddress").value;
             var sjaddresscheck1=sjaddress=="";
             var sjaddresscheck2=/^[a-zA-Z\u4e00-\u9fa5\s\d]{1,20}$/;
             if(sjaddresscheck1){
             	 $("#hidesjaddress").html("<label>*请输入商家地址</label>")
             }
             else if(!sjaddresscheck2.test(sjaddress))
				 {  
					$("#hidesjaddress").html("<label>*请输入正确格式的商家地址</label>")
				}
            else{
                $("#hidesjaddress").children().empty();
            }
        })
})
//商家电话
$(function(){
			$("#sjtel").blur(function(){
			var sjtel = document.getElementById("sjtel").value;
			var mobile = /^1[3|5|8]\d{9}$/;
			var phone = /^0\d{2,3}-?\d{7,8}$/;
			var sjtelcheck1=sjtel=="";
			var sjtelcheck2=mobile.test(sjtel)||phone.test(sjtel);
			    if(sjtelcheck1){
				       $("#hidesjtel").html("<label>*请输入商家电话</label>")
				}
				else if(!sjtelcheck2) 
					    {  
						$("#hidesjtel").html("<label>*请输入正确格式的电话</label>")
						}
                else{
                        $("#hidesjtel").children().empty();
                   }
			})
});

//验证传真XXX(X)-XXXXXXX(X)
$(function(){
		
			$("#sjfax").blur(function(){
			var sjfax = document.getElementById("sjfax").value;
			var fax=/^(\d{3,4}-)?\d{7,8}$/;
			var sjfaxcheck1=sjfax=="";
			var sjfaxcheck2=fax.test(sjfax);
			    if(sjfaxcheck1){
				     $("#hidesjfax").html("<label>*请输入商家传真</label>")
				}
				else if(!sjfaxcheck2) 
					    {  
						$("#hidesjfax").html("<label>*请输入正确格式的传真</label>")
						}
				else{
                        $("#hidesjfax").children().empty();
                   }	
			})
});
//验证商家信息维护btn
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`~~~~~~~~~产品信息维护——酒店类~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//验证产品名称（中文、英文、空格、数字）
$(function(){
        $("#productName").blur(function(){
             var productName=document.getElementById("productName").value;
             var productnamecheck1=productName=="";
             var productnamecheck2=/^[a-zA-Z\u4e00-\u9fa5\s\d]{1,20}$/;
             if(productnamecheck1){
             	 $("#hideproductname").html("<label>*请输入产品名称</label>")
             }else if(!productnamecheck2.test(productName))
				 {  
					$("#hideproductname").html("<label>*请输入正确格式的产品名称</label>")
			}else{
                        $("#hideproductname").children().empty();
                   }   
        })
})
//验证门市价格（只能是数字）
$(function(){
        $("#productfPrice").blur(function(){
             var productfPrice=document.getElementById("productfPrice").value;
             var productfPricecheck1=productfPrice=="";
             var productfPricecheck2=isNaN(productfPrice);
             if(productfPricecheck1){
             	 $("#hideproductfPrice").html("<label>*请输入产品门市价格</label>")
             }
             else if(productfPricecheck2)
				 {  
					$("#hideproductfPrice").html("<label>*请输入正确格式的门市价格</label>")
				}
             else{
                       $("#hideproductfPrice").children().empty();
                   }   
        })
})
//验证优惠价格（只能是数字）
$(function(){
        $("#productfASPOTPrice").blur(function(){
             var productfASPOTPrice=document.getElementById("productfASPOTPrice").value;
             var productfASPOTPricecheck1=productfASPOTPrice=="";
             var productfASPOTPricecheck2=isNaN(productfASPOTPrice);
             if(productfASPOTPricecheck1){
             	 $("#hideproductfASPOTPrice").html("<label>*请输入产品优惠价格</label>")
             }
             else if(productfASPOTPricecheck2)
				 {  
					$("#hideproductfASPOTPrice").html("<label>*请输入正确格式的优惠价格</label>")
				}
             else{
                        $("#hideproductfASPOTPrice").children().empty();
                   } 
        })
})
//验证床型
$(function(){
        $("#productstrBedType").blur(function(){
             var productstrBedType=document.getElementById("productstrBedType").value;
             var productstrBedTypecheck1=productstrBedType=="";
              var productstrBedTypecheck2=productstrBedType!="";
             if(productstrBedTypecheck1){
             	 $("#hideproductstrBedType").html("<label>*请输入床型</label>")
             	 $("#hideproductstrBedType").show();
             }
             else if(productstrBedTypecheck2){
             	 $("#hideproductstrBedType").children().empty();
             }
            

        })
})
//验证房型
$(function(){
        $("#productstrRoomType").blur(function(){
             var productstrRoomType=document.getElementById("productstrRoomType").value;
             var productstrRoomTypecheck1=productstrRoomType=="";
              var productstrRoomTypecheck2=productstrRoomType!="";
             if(productstrRoomTypecheck1){
             	 $("#hideproductstrRoomType").html("<label>*请输入房型</label>")
             	 $("#hideproductstrRoomType").show();
             }
             else if(productstrRoomTypecheck2){
             	 $("#hideproductstrRoomType").children().empty();
             }
        })
})
//验证早餐
$(function(){
        $("#productstrBreakfast").blur(function(){
             var productstrBreakfast=document.getElementById("productstrBreakfast").value;
             var productstrBreakfastcheck1=productstrBreakfast=="";
              var productstrBreakfastcheck2=productstrBreakfast!="";
             if(productstrBreakfastcheck1){
             	 $("#hideproductstrBreakfast").html("<label>*请输入早餐</label>")
             	 $("#hideproductstrBreakfast").show();
             }
             else if(productstrBreakfastcheck2){
             	 $("#hideproductstrBreakfast").children().empty();
             }
           
        })
})
//验证宽带
$(function(){
        $("#productstrwifi").blur(function(){
             var productstrwifi=document.getElementById("productstrwifi").value;
             var productstrwificheck1=productstrwifi=="";
              var productstrwificheck2=productstrwifi!="";
             if(productstrwificheck1){
             	 $("#hideproductstrwifi").html("<label>*请输入宽带</label>")
             	 $("#hideproductstrwifi").show();
             }
             else if(productstrwificheck2){
             	 $("#hideproductstrwifi").children().empty();
             }

        })
})
//验证产品说明
$(function(){
        $("#productstrRemarks").blur(function(){
             var productstrRemarks=document.getElementById("productstrRemarks").value;
             var productstrRemarkscheck1=productstrRemarks=="";
              var productstrRemarkscheck2=productstrRemarks!="";
             if(productstrRemarkscheck1){
             	 $("#hideproductstrRemarks").html("<label>*请输入产品说明</label>")
             	 $("#hideproductstrRemarks").show();
             }
             else if(productstrRemarkscheck2){
             	 $("#hideproductstrRemarks").children().empty();
             }
        })
})
//确认添加AddOKbtn
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`~~~~~~~~~产品信息维护——非酒店类~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//验证产品名称（中文、英文、空格、数字）
$(function(){
        $("#product1Name").blur(function(){
        	alert("ashdaosewur")
             var product1Name=document.getElementById("product1Name").value;
             var product1namecheck1=product1Name=="";
             var product1namecheck2=/^[a-zA-Z\u4e00-\u9fa5\s\d]{1,20}$/;
             if(product1namecheck1){
             	 $("#hideproduct1name").html("<label>*请输入产品名称</label>")
             }
             else if(!product1namecheck2.test(product1Name))
				 {  
					$("#hideproduct1name").html("<label>*请输入正确格式的产品名称</label>")
				}
        })
})
//验证门市价格（只能是数字）
$(function(){
        $("#product1fPrice").blur(function(){
             var product1fPrice=document.getElementById("product1fPrice").value;
             var product1fPricecheck1=product1fPrice=="";
             var product1fPricecheck2=isNaN(product1fPrice);
             if(product1fPricecheck1){
             	 $("#hideproduct1fPrice").html("<label>*请输入产品门市价格</label>")
             }
             else if(product1fPricecheck2)
				 {  
					$("#hideproduct1fPrice").html("<label>*请输入正确格式的门市价格</label>")
				}
        })
})
//验证优惠价格（只能是数字）
$(function(){
        $("#product1fASPOTPrice").blur(function(){
             var product1fASPOTPrice=document.getElementById("product1fASPOTPrice").value;
             var product1fASPOTPricecheck1=product1fASPOTPrice=="";
             var product1fASPOTPricecheck2=isNaN(product1fASPOTPrice);
             if(product1fASPOTPricecheck1){
             	 $("#hideproduct1fASPOTPrice").html("<label>*请输入产品优惠价格</label>")
             }
             else if(product1fASPOTPricecheck2)
				 {  
					$("#hideproduct1fASPOTPrice").html("<label>*请输入正确格式的优惠价格</label>")
				}
        })
})
//验证产品说明
$(function(){
        $("#product1strRemarks").blur(function(){
             var product1strRemarks=document.getElementById("product1strRemarks").value;
             var product1strRemarkscheck1=product1strRemarks=="";
              var product1strRemarkscheck2=product1strRemarks!="";
             if(product1strRemarkscheck1){
             	 $("#hideproduct1strRemarks").html("<label>*请输入产品说明</label>")
             	 $("#hideproduct1strRemarks").show();
             }
             else if(product1strRemarkscheck2){
             	 $("#hideproduct1strRemarks").hide();
             }

        })
})
//确认添加AddOKbtn
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~预制卡制作~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//验证预制卡数量（只能是0~10数字）
$(function(){
        $("#yzkNum").blur(function(){
             var yzkNum=document.getElementById("yzkNum").value;
             var yzkNumcheck1=yzkNum=="";
             var yzkNumcheck2= /^\d+$/.test(yzkNum)&&yzkNum<=10&&yzkNum!=0;
             if(yzkNumcheck1){
             	 $("#hideyzkNum").html("<label>*请输入预制卡数量</label>")
             }
             else if(!yzkNumcheck2)
				 {  
					$("#hideyzkNum").html("<label>*请输入1~10的数字</label>");
					$("#hideyzkNum").show();
				}
			else if(yzkNumcheck2)
				$("#hideyzkNum").children().empty();
        })
})
//yzkNum_submit_btn确认预制卡制作按钮
$(function(){
             $("#yzkNumsubmitbtn").click(function () {
            var yzkNum=document.getElementById("yzkNum").value;
             var yzkNumcheck1=yzkNum=="";
             var yzkNumcheck2= /^\d+$/.test(yzkNum)&&yzkNum<=10&&yzkNum!=0;
             if(yzkNumcheck1){
                 $("#hideyzkNum").html("<label>*请输入预制卡数量</label>")
             }
             else if(!yzkNumcheck2)
                 {  
                    $("#hideyzkNum").html("<label>*请输入1~10的数字</label>");
                    $("#hideyzkNum").show();
                }
            else if(yzkNumcheck2)
                $("#writeincard").fadeIn(1000);
        })
})
//写入磁卡按钮1

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~现场制卡~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//点我生成一个卡号
$(function(){
		     $("#borncard_btn").click(function () {
			    $("#kahao").text("0003201407280001")
			    })
		    })
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~会员卡发放~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//会员姓名验证（中文字符）
$(function(){

        $("#vipnamefafang").blur(function(){

             var vipnamefafang=document.getElementById("vipnamefafang").value;
             var vipnamefafangcheck1=vipnamefafang=="";
             var vipnamefafangcheck2= /^([\u4E00-\u9FA5])*$/.test(vipnamefafang)&&vipnamefafang.length>1&&vipnamefafang.length<=4;
             if(vipnamefafangcheck1){
             	 $("#hidevipnamefafang").html("<label>*请输入会员姓名</label>")
             }
             else if(!vipnamefafangcheck2)
				 {  
					$("#hidevipnamefafang").html("<label>*请输入会员真实姓名</label>");
					$("#hidevipnamefafang").show();
				}
			else if(vipnamefafangcheck2)
				$("#hidevipnamefafang").children().empty();
        })
})

//会员身份证号验证
$(function(){

        $("#idnumfafang").blur(function(){

             var idnumfafang=document.getElementById("idnumfafang").value;
             var idnumfafangcheck1=idnumfafang=="";
             var idnumfafangcheck2= /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(idnumfafang);
             if(idnumfafangcheck1){
             	 $("#hideidnamefafang").html("<label>*请输入会员身份证号</label>")
             }
             else if(!idnumfafangcheck2)
				 {  
					$("#hideidnamefafang").html("<label>*请输入有效的身份证号</label>");
					$("#hideidnamefafang").show();
				}
			else if(idnumfafangcheck2)
				$("#hideidnamefafang").children().empty();
        })
})
//将卡放在读卡机上
$(function(){
		     $("#putcardatmachine").click(function () {
			    $("#duka").text("读卡成功")
			    })
		    })
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~会员补卡~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//会员姓名验证（中文字符）
$(function(){

        $("#vipnamebuka").blur(function(){
             var vipnamebuka=document.getElementById("vipnamebuka").value;
             var vipnamebukacheck1=vipnamebuka=="";
             var vipnamebukacheck2= /^([\u4E00-\u9FA5])*$/.test(vipnamebuka)&&vipnamebuka.length>1&&vipnamebuka.length<=4;
             if(vipnamebukacheck1){
             	 $("#hidevipnamebuka").html("<label>*请输入会员姓名</label>")
             }
             else if(!vipnamebukacheck2)
				 {  
					$("#hidevipnamebuka").html("<label>*请输入会员真实姓名</label>");
					$("#hidevipnamebuka").show();
				}
			else if(vipnamebukacheck2)
				$("#hidevipnamebuka").children().empty();
        })
})
//会员身份证号验证
$(function(){

        $("#idnumbuka").blur(function(){

             var idnumbuka=document.getElementById("idnumbuka").value;
             var idnumbukacheck1=idnumfafang=="";
             var idnumbukacheck2= /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(idnumbuka);
             if(idnumbukacheck1){
             	 $("#hideidnumbuka").html("<label>*请输入会员身份证号</label>")
             }
             else if(!idnumbukacheck2)
				 {  
					$("#hideidnumbuka").html("<label>*请输入有效的身份证号</label>");
					$("#hideidnumbuka").show();
				}
			else if(idnumbukacheck2)
				$("#hideidnumbuka").children().empty();
        })
})
//将卡放在读卡机上
$(function(){
		     $("#putcardatmachine1").click(function () {
			    $("#duka1").text("读卡成功")
			    })
		    })
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~会员信息添加~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//验证会员姓名（中文字符2~4位）

$(function(){

        $("#vipinfoname").blur(function(){
             var vipinfoname=document.getElementById("vipinfoname").value;
             var vipinfonamecheck1=vipinfoname=="";
             var vipinfonamecheck2= /^([\u4E00-\u9FA5])*$/.test(vipinfoname)&&vipinfoname.length>1&&vipinfoname.length<=4;
             if(vipinfonamecheck1){
             	 $("#hidevipinfoname").html("<label>*请输入会员姓名</label>")
             }
             else if(!vipinfonamecheck2)
				 {  
					$("#hidevipinfoname").html("<label>*请输入会员真实姓名</label>");
					$("#hidevipinfoname").show();
				}
			else if(vipinfonamecheck2)
				$("#hidevipinfoname").children().empty();
        })
})
//验证会员卡卡号（非数字）
$(function(){
        $("#vipinfocardnum").blur(function(){
             var vipinfocardnum=document.getElementById("vipinfocardnum").value;
             var vipinfocardnumcheck1=vipinfocardnum=="";
             var vipinfocardnumcheck2=isNaN(vipinfocardnum);
             if(vipinfocardnumcheck1){
             	 $("#hidevipinfocardnum").html("<label>*请输入会员卡卡号</label>")
             }
             else if(vipinfocardnumcheck2)
				 {  
					$("#hidevipinfocardnum").html("<label>*请输入正确格式的会员卡卡号</label>")
				}
             else {
                $("#hidevipinfocardnum").children().empty();   
            }
        })

})
//会员身份证号验证
$(function(){

        $("#infocardnum").blur(function(){

             var infocardnum=document.getElementById("infocardnum").value;
             var infocardnumcheck1=infocardnum=="";
             var infocardnumcheck2= /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(infocardnum);
             if(infocardnumcheck1){
             	 $("#hideinfocardnum").html("<label>*请输入会员身份证号</label>")
             }
             else if(!infocardnumcheck2)
				 {  
					$("#hideinfocardnum").html("<label>*请输入有效的身份证号</label>");
					$("#hideinfocardnum").show();
				}
			else if(infocardnumcheck2)
				$("#hideinfocardnum").children().empty();   
        })
})
//手机号
$(function(){
            $("#vipinfophonenum").blur(function(){
            
             var vipinfophonenum=document.getElementById("vipinfophonenum").value;
             var vipinfophonenumcheck1=vipinfophonenum=="";
             var vipinfophonenumcheck2=/^1[3|5|8]\d{9}$/.test(vipinfophonenum);
             if(vipinfophonenumcheck1){
                 $("#hidevipinfophonenum").html("<label>*请输入会员手机号</label>")
             }
             else if(!vipinfophonenumcheck2)
                 {  
                    $("#hidevipinfophonenum").html("<label>*请输入有效的手机号</label>");
                    $("#hidevipinfophonenum").show();
                }
            else if(vipinfophonenumcheck2)
                $("#hidevipinfophonenum").children().empty();   
        })
});
//会员电子邮箱
$(function(){
   
            $("#vipinfomail").blur(function(){
            
             var vipinfomail=document.getElementById("vipinfomail").value;
             var vipinfomailcheck1=vipinfomail=="";
             var vipinfomailcheck2= /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(vipinfomail);
             if(vipinfomailcheck1){
                 $("#hidevipinfomail").html("<label>*请输入会员电子邮箱</label>")
             }
             else if(!vipinfomailcheck2)
                 {  
                    $("#hidevipinfomail").html("<label>*请输入有效的邮箱</label>");
                    $("#hidevipinfomail").show();
                }
            else if(vipinfomailcheck2)
                $("#hidevipinfomail").children().empty();   
        })
});
//会员地址（中、英、空格、数字）
$(function(){
        $("#vipinfoaddress").blur(function(){
           
             var vipinfoaddress=document.getElementById("vipinfoaddress").value;
             var vipinfoaddresscheck1=vipinfoaddress=="";
             var vipinfoaddresscheck2=/^[a-zA-Z\u4e00-\u9fa5\s\d]{1,20}$/;
             if(vipinfoaddresscheck1){
                 $("#hidevipinfoaddress").html("<label>*请输入商家地址</label>")
             }
             else if(!vipinfoaddresscheck2.test(vipinfoaddress))
                 {  
                    $("#hidevipinfoaddress").html("<label>*请输入正确格式的商家地址</label>")
                }
            else if(vipinfoaddresscheck2)
                $("#hidevipinfoaddress").children().empty();   

        })
})


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~会员信息更改~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//原手机号
$(function(){
            $("#vipphonenum").blur(function(){
            
             var vipphonenum=document.getElementById("vipphonenum").value;
             var vipphonenumcheck1=vipphonenum=="";
             var vipphonenumcheck2=/^1[3|5|8]\d{9}$/.test(vipphonenum);
             if(vipphonenumcheck1){
                 $("#hidevipphonenum").html("<label>*请输入会员手机号</label>")
             }
             else if(!vipphonenumcheck2)
                 {  
                    $("#hidevipphonenum").html("<label>*请输入有效的手机号</label>");
                    $("#hidevipphonenum").show();
                }
            else if(vipphonenumcheck2)
                $("#hidevipphonenum").children().empty();   
        })
});
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~会员变更历史~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//原手机号
$(function(){
            $("#vipphonenum").blur(function(){
            
             var vipphonenum=document.getElementById("vipphonenum").value;
             var vipphonenumcheck1=vipphonenum=="";
             var vipphonenumcheck2=/^1[3|5|8]\d{9}$/.test(vipphonenum);
             if(vipphonenumcheck1){
                 $("#hidevipphonenum").html("<label>*请输入会员手机号</label>")
             }
             else if(!vipphonenumcheck2)
                 {  
                    $("#hidevipphonenum").html("<label>*请输入有效的手机号</label>");
                    $("#hidevipphonenum").show();
                }
            else if(vipphonenumcheck2)
                $("#hidevipphonenum").children().empty();
        })
});
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~变更历史统计~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//原手机号
$(function(){
            $("#vipphonenum1").blur(function(){
            
             var vipphonenum1=document.getElementById("vipphonenum1").value;
             var vipphonenum1check1=vipphonenum1=="";
             var vipphonenum1check2=/^1[3|5|8]\d{9}$/.test(vipphonenum1);
             if(vipphonenum1check1){
                 $("#hidevipphonenum1").html("<label>*请输入会员手机号</label>")
             }
             else if(!vipphonenum1check2)
                 {  
                    $("#hidevipphonenum1").html("<label>*请输入有效的手机号</label>");
                    $("#hidevipphonenum1").show();
                }
            else if(vipphonenum1check2)
                $("#hidevipphonenum1").children().empty();
        })
});
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~会员充值~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//充值金额（数字）
$(function(){
        $("#chargemoney").blur(function(){
             var chargemoney=document.getElementById("chargemoney").value;
             var chargemoneycheck1=chargemoney=="";
             var chargemoneycheck2=isNaN(chargemoney);
             if(chargemoneycheck1){
                 $("#hidechargemoney").html("<label>*请输入充值金额</label>")
             }
             else if(chargemoneycheck2)
                 {  
                    $("#hidechargemoney").html("<label>*请输入正确格式的金额</label>")
                }
            else if(!chargemoneycheck2)
                 $("#hidechargemoney").children().empty();
        })
})
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~充值历史查询~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//原手机号
$(function(){
            $("#vipphonenum2").blur(function(){
             var vipphonenum2=document.getElementById("vipphonenum2").value;
             var vipphonenum2check1=vipphonenum2=="";
             var vipphonenum2check2=/^1[3|5|8]\d{9}$/.test(vipphonenum2);
             if(vipphonenum2check1){
                 $("#hidevipphonenum2").html("<label>*请输入会员手机号</label>")
             }
             else if(!vipphonenum2check2)
                 {  
                    $("#hidevipphonenum2").html("<label>*请输入有效的手机号</label>");
                    $("#hidevipphonenum2").show();
                }
            else if(vipphonenum2check2)
                $("#hidevipphonenum2").children().empty();
        })
});
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~现金消费~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//消费金额
$(function(){
        $("#shoppingmoney").blur(function(){
             var shoppingmoney=document.getElementById("shoppingmoney").value;
             var shoppingmoneycheck1=shoppingmoney=="";
             var shoppingmoneycheck2=isNaN(shoppingmoney);
             if(shoppingmoneycheck1){
                 $("#hideshoppingmoney").html("<label>*请输入充值金额</label>")
             }
             else if(shoppingmoneycheck2)
                 {  
                    $("#hideshoppingmoney").html("<label>*请输入正确格式的金额</label>")
                    $("#hideshoppingmoney").show();
                }
            else if(!shoppingmoneycheck2)
                 $("#hideshoppingmoney").children().empty();
        })
})
//将卡放在读卡机上
$(function(){
             $("#putcardatmachine2").click(function () {
                $("#duka2").text("读卡成功")
                })
            })
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~联盟消费~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//消费金额
$(function(){
        $("#shoppingmoney1").blur(function(){
             var shoppingmoney1=document.getElementById("shoppingmoney1").value;
             var shoppingmoney1check1=shoppingmoney1=="";
             var shoppingmoney1check2=isNaN(shoppingmoney1);
             if(shoppingmoney1check1){
                 $("#hideshoppingmoney1").html("<label>*请输入充值金额</label>")
             }
             else if(shoppingmoney1check2)
                 {  
                    $("#hideshoppingmoney1").html("<label>*请输入正确格式的金额</label>")
                    $("#hideshoppingmoney1").show();
                }
            else if(!shoppingmoney1check2)
                 $("#hideshoppingmoney1").children().empty();
        })
})
//将卡放在读卡机上
$(function(){
             $("#putcardatmachine3").click(function () {
                $("#duka3").text("读卡成功")
                })
            })
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~会员充值~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//验证门市价格（只能是数字）
$(function(){
        $("#rechargevipid").blur(function(){
             var rechargevipid=document.getElementById("rechargevipid").value;
             var rechargevipidcheck1=rechargevipid=="";
             var rechargevipidcheck2=isNaN(rechargevipid);
             if(rechargevipidcheck1){
                 $("#hiderechargevipid").html("<label>*请输入会员ID</label>")
             }
             else if(rechargevipidcheck2)
                 {  
                    $("#hiderechargevipid").html("<label>*请输入正确格式的会员ID</label>")
                }
             else{
                       $("#hiderechargevipid").children().empty();
                   }   
        })
})